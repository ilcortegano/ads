#include "common/common.hpp"
#include <queue>


int main() {
	const size_t ADS_SIZE = sizeof(std::queue<int>);
	ilc::csv::CsvWriter csv;
	std::queue<int> queue;

	BENCHMARK_BEGIN("std_push.csv")
	{
		queue.push(0);
	}
	BENCHMARK_STD_CSV_WRITE()
	BENCHMARK_END("push")

	BENCHMARK_BEGIN("std_pop.csv")
	{
		queue.pop();
	}
	BENCHMARK_STD_CSV_WRITE()
	BENCHMARK_END("pop")
	return 0;
}
