#include "common/testing/engine.hpp"
#include "common/testing/asserts.hpp"
#include "queue/queue.hpp"
using namespace ilc::test;
using namespace ilc::ads;

template<typename T>
struct FakeQueue {
	T* array;
	size_t begin;
	size_t end;
	size_t elems;
	size_t capacity;
};


void testEmpty() {
	Queue<size_t> container;

	assert(container.empty());
	assert(!container.full());
	assert(container.size() == 0);
	assert(container.capacity() == INITIAL_CAPACITY);
	assert_exception_thrown(container.front());
	assert_exception_thrown(container.back());

	container.pop();
	assert(container.size() == 0);
	assert_exception_thrown(container.front());
	assert_exception_thrown(container.back());
}

void testFull() {
	Queue<size_t> container;
	FakeQueue<size_t>* manipulator = reinterpret_cast<FakeQueue<size_t>*>(&container);
	manipulator->elems = MAX_CAPACITY;

	assert(!container.empty());
	assert(container.full());
	assert(container.size() == MAX_CAPACITY);
	assert_exception_thrown(container.push(0));
}

void testPushAndPop() {
	const size_t N = INITIAL_CAPACITY + 1;
	Queue<size_t> container;

	for(size_t i = 0; i < N; i++) {
		container.push(i);
		assert(container.front() == 0);
		assert(container.back() == i);
	}
	assert(!container.empty());
	assert(container.size() == N);
	assert(container.capacity() == INITIAL_CAPACITY * 2);

	for(size_t i = 1; i <= N; i++) {
		assert(container.front() == i - 1);
		container.pop();
	}
	assert(container.empty());
	assert(container.size() == 0);
	assert(container.capacity() == 2);
}

void testFit() {
	size_t N = INITIAL_CAPACITY + 1;
	Queue<size_t> containerA;

	for(size_t i = 0; i < N; i++) {
		containerA.push(i);
	}
	containerA.fit();

	assert(containerA.size() == N);
	assert(containerA.capacity() == N);

	N = 256;
	Queue<size_t> containerB;
	for (size_t i = 0; i < N; i++)  { containerB.push(i); }
	for (size_t i = 0; i < 32; i++) { containerB.pop();   }
	for (size_t i = 0; i < 32; i++) { containerB.push(i); }
	containerB.fit();

	assert(containerB.size() == N);
	assert(containerB.capacity() == N);

	for (size_t i = 32; i < N; i++) {
		assert(containerB.front() == i);
		containerB.pop();
	}
	for (size_t i = 0; i < 32; i++) {
		assert(containerB.front() == i);
		containerB.pop();
	}
	assert(containerB.size() == 0);
	assert(containerB.capacity() == 2);
}

void testClear() {
	const size_t N = INITIAL_CAPACITY + 1;
	Queue<size_t> container;

	for(size_t i = 0; i < N; i++) {
		container.push(i);
	}
	container.clear();

	assert(container.size() == 0);
	assert(container.capacity() == (INITIAL_CAPACITY * 2));
}

void testSwap() {
	const size_t N = 256;
	Queue<size_t> containerA;
	Queue<size_t> containerB;
	size_t sizeA = rand() % N;
	size_t sizeB = rand() % N;

	for (size_t i = 0; i < sizeA; i++) { containerA.push(i); }
	for (size_t i = 0; i < sizeB; i++) { containerB.push(sizeB - i); }
	containerA.swap(containerB);

	assert(containerA.size() == sizeB);
	assert(containerA.front() ==  sizeB);
	assert(containerB.size() == sizeA);
	assert(containerB.front() == 0);
}

void testCopyCtor() {
	const size_t N = INITIAL_CAPACITY + 1;
	Queue<size_t> containerA;

	for (size_t i = 0; i < N; i++) {
		containerA.push(i);
	}

	Queue<size_t> containerB(containerA);
	assert(!containerB.full());
	assert(!containerB.empty());
	assert(containerB.size() == N);
	assert(containerB.capacity() == INITIAL_CAPACITY * 2);

	for (size_t i = 0; i < N; i++) {
		assert(containerB.front() == i);
		assert(containerB.back() == N - 1);
		containerB.pop();
	}
}

void testAsignation() {
	const size_t N = 256;
	Queue<size_t> containerA;

	for (size_t i = 0; i < N; i++) {
		containerA.push(i);
	}
	containerA = containerA;
	Queue<size_t> containerB = containerA;

	assert(!containerB.full());
	assert(!containerB.empty());
	assert(containerB.size() == N);
	assert(containerB.capacity() == N);

	for (size_t i = 0; i < N; i++) {
		assert(containerB.front() == containerA.front());
		containerB.pop();
		containerA.pop();
	}
	assert(containerA.empty());
	assert(containerB.empty());
}

void testEquals() {
	Queue<size_t> containerA, containerB;

	assert(containerA == containerB);
	assert(!(containerA != containerB));

	containerA.push(0);
	assert(containerA != containerB);

	containerB.push(0);
	assert(containerA == containerB);

	containerA.push(1);
	assert(containerA != containerB);

	containerB.pop();
	containerB.push(0);
	containerB.push(1);
	assert(containerA == containerB);
}

void testAlphanumericOrder() {
	Queue<size_t> containerA, containerB;

	assert(containerA >= containerB);
	assert(containerA <= containerB);
	assert(!(containerA < containerB));
	assert(!(containerA > containerB));

	containerA.push(0);
	assert(containerA > containerB);

	containerB.push(1);
	assert(containerB > containerA);

	containerA.pop();
	containerA.push(0);
	containerA.push(1);
	assert(containerA > containerB);

	containerB.clear();
	containerB.push(0);
	containerB.push(1);
	assert(!(containerA > containerB));
	assert(!(containerA < containerB));
	assert(containerA == containerB);
}

int main() {
	TestEngine::execute("1", &testEmpty);
	TestEngine::execute("2", &testFull);
	TestEngine::execute("3", &testPushAndPop);
	TestEngine::execute("4", &testFit);
	TestEngine::execute("5", &testClear);
	TestEngine::execute("6", &testSwap);
	TestEngine::execute("7", &testCopyCtor);
	TestEngine::execute("8", &testAsignation);
	TestEngine::execute("9", &testEquals);
	TestEngine::execute("10", &testAlphanumericOrder);
	TestEngine::printResults();
	return 0;
}
