#pragma once
#include <stdexcept>

namespace ilc::ads {

	constexpr size_t INITIAL_CAPACITY = 8;
	constexpr size_t MAX_CAPACITY = 9223372036854775808ULL;

	template <class T>
	class Queue {

	public:
		//------------------------------------//
		//     CONSTRUCTORS & DESTRUCTOR      //
		//------------------------------------//
		Queue(): _array(new T[INITIAL_CAPACITY]), _capacity(INITIAL_CAPACITY),
				 _elems(0), _begin(0), _end(0) {}

		Queue(const Queue &other) {
			copy(other);
		}

		~Queue() {
			delete[] _array;
		}

		//------------------------------------//
		//             MODIFIERS              //
		//------------------------------------//
		void push(const T &elem) {
			if (_elems == MAX_CAPACITY) throw std::overflow_error("");
			if (_elems == _capacity)
				resize(_capacity << 1);

			if (_elems != 0) {
				_end = (_end == _capacity - 1)? 0: _end + 1;
			}
			_array[_end] = elem;
			_elems++;
		}

		void pop() {
			if (_elems > 1) {
				_begin = (_begin == _capacity - 1)? 0: _begin + 1;
			}
			_elems = _elems - (_elems != 0);

			if (_elems < (_capacity >> 2))
				resize(_capacity >> 1);
		}

		void fit() {
			if (_elems == 0)
				return;

			resize(_elems);
		}

		void clear() {
			_elems = 0;
			_begin = 0;
			_end = 0;
		}

		void swap(Queue& other) {
			T* array {_array};
			size_t capacity {_capacity};
			size_t elems {_elems};
			size_t begin {_begin};
			size_t end {_end};

			_array = other._array;
			_capacity = other._capacity;
			_elems = other._elems;
			_begin = other._begin;
			_end = other._end;

			other._array = array;
			other._capacity = capacity;
			other._elems = elems;
			other._begin = begin;
			other._end = end;
		}

		//------------------------------------//
		//             OBSERVERS              //
		//------------------------------------//
		const T &front() const {
			if (_elems == 0) throw std::out_of_range("");
			return _array[_begin];
		}

		const T &back() const {
			if (_elems == 0) throw std::out_of_range("");
			return _array[_end];
		}

		size_t size() const {
			return _elems;
		}

		size_t capacity() const {
			return _capacity;
		}

		size_t memorySpace() const {
			return sizeof(Queue) + sizeof(T) * _capacity;
		}

		bool empty() const {
			return _elems == 0;
		}

		bool full() const {
			return _elems == MAX_CAPACITY;
		}

		bool operator==(const Queue &other) const {
			size_t i = 0;

			if (_elems != other._elems)
				return false;

			size_t p1 = _begin;
			size_t p2 = other._begin;
			while ((i != _elems) && (_array[p1] == other._array[p2])) {
				++i;
				p1 = (_begin + i) % _capacity;
				p2 = (other._begin + i) % other._capacity;
			}

			return (i == _elems);
		}

		bool operator!=(const Queue &other) const {
			return !(*this == other);
		}

		bool operator<(const Queue &other) const {
			size_t i = 0;
			size_t p1 = _elems - 1;
			size_t p2 = other._elems - 1;

			while (i < _elems) {
				if (other._elems <= i || other._array[p2] < _array[p1]) return false;
				else if (_array[p1] < other._array[p2]) return true;
				i++;
				p1--;
				p2--;
			}
			return (other._elems > i);
		}

		bool operator>(const Queue &other) const {
			return other < *this;
		}

		bool operator<=(const Queue &other) const {
			return !(other < *this);
		}

		bool operator>=(const Queue &other) const {
			return !(*this < other);
		}

		//------------------------------------//
		//             OPERATORS              //
		//------------------------------------//
		Queue &operator=(const Queue &other) {
			if (this != &other) {
				Queue tmp{other};
				swap(tmp);
			}
			return *this;
		}

	private:
		T *_array;
		size_t _begin;
		size_t _end;
		size_t _elems;
		size_t _capacity;

		void resize(size_t size) {
			T *old = _array;

			_capacity = size;
			_array = new T[size];

			if (_begin <= _end) {
				memcpy(_array, old + _begin, _elems * sizeof(T));
			}
			else {
				size_t elemsAtEnd = _capacity - _begin;
				memcpy(_array, old + _begin, elemsAtEnd * sizeof(T));
				memcpy(_array + elemsAtEnd, old, (_end + (_elems != 0)) * sizeof(T));
			}
			_begin = 0;
			_end = (_elems == 0)? 0: _elems - 1;
			delete[] old;
		}

		void copy(const Queue &other) {
			_begin = other._begin;
			_end = other._end;
			_elems = other._elems;
			_capacity = other._capacity;
			_array = new T[other._capacity];

			memcpy(_array, other._array, other._capacity * sizeof(T));
		}
	};
};