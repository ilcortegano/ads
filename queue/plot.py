import sys
import os

current = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.dirname(current))

from common import plot, Config

config = Config()
config.BASE_PATH = current
config.ADS = 'queue'
config.STD_ADS = config.ADS
config.OPERATION = 'push'
config.STD_OPERATION = config.OPERATION
plot(config)

config.OPERATION = 'pop'
config.STD_OPERATION = config.OPERATION
plot(config)