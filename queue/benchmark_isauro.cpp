#include "common/common.hpp"
#include "queue/queue.hpp"


int main() {
	ilc::csv::CsvWriter csv;
	ilc::ads::Queue<int> queue;

	BENCHMARK_BEGIN("isauro_push.csv")
	{
		queue.push(0);
	}
	BENCHMARK_CSV_WRITE() << queue.memorySpace() << "\n";
	BENCHMARK_END("push")

	BENCHMARK_BEGIN("isauro_pop.csv")
	{
		queue.pop();
	}
	BENCHMARK_CSV_WRITE() << queue.memorySpace() << "\n";
	BENCHMARK_END("pop")
	return 0;
}
