import sys
import os

current = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.dirname(current))

from common import plot, Config

config = Config()
config.BASE_PATH = current
config.ADS = 'set'
config.STD_ADS = config.ADS
config.MATH_FN = 'log_n'
config.OPERATION = 'insert'
config.STD_OPERATION = config.OPERATION
plot(config)

config.MATH_FN = None
config.OPERATION = 'erase'
config.STD_OPERATION = config.OPERATION
plot(config)