#include "common/common.hpp"
#include "set/set.hpp"


int main() {
	ilc::csv::CsvWriter csv;
	ilc::ads::Set<size_t> tree;

	BENCHMARK_BEGIN("isauro_insert.csv")
	{
		tree.insert(i * M + j);
	}
	BENCHMARK_CSV_WRITE() << tree.memorySpace() << "\n";
	BENCHMARK_END("insert")

	BENCHMARK_BEGIN("isauro_erase.csv")
	{
		tree.erase(i * M + j);
	}
	BENCHMARK_CSV_WRITE() << tree.memorySpace() << "\n";
	BENCHMARK_END("erase")
	return 0;
}
