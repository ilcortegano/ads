#include "common/testing/engine.hpp"
#include "common/testing/asserts.hpp"
#include "set/set.hpp"
using namespace ilc::test;
using namespace ilc::ads;


template<class T>
class FakeSet {

public:

	struct FakeSetNode {
		T elem;
		bool color;
		FakeSetNode *left;
		FakeSetNode *right;
	};

	FakeSetNode *root;
	size_t elems;
};


void testEmpty() {
	Set<size_t> container;

	assert(container.empty());
	assert(!container.full());
	assert(container.size() == 0);

	assert_false(container.contains(3));

	assert_exception_not_thrown(container.deleteMax());
	assert_exception_not_thrown(container.deleteMin());
	assert_exception_not_thrown(container.erase(3));
}

void testFull() {
	Set<size_t> container;
	FakeSet<size_t>* manipulator = reinterpret_cast<FakeSet<size_t>*>(&container);
	manipulator->elems = SIZE_MAX;

	assert(!container.empty());
	assert(container.full());
	assert(container.size() == SIZE_MAX);

	assert_exception_thrown(container.insert(3));
}

void testInsertDuplicates() {
	const size_t N = 2048;
	Set<size_t> container;

	for(size_t i = 0; i < N; i++)
		container.insert(0);

	assert(!container.empty());
	assert(container.size() == 1);
	assert_true(container.contains(0));
}

void testInsertAndDeleteMin() {
	const size_t N = 64;
	Set<size_t> container;

	for(size_t i = 0; i < N; i++)
		container.insert(i);


	assert(!container.empty());
	assert(container.size() == N);

	for(size_t i = 0; i < N; i++) {
		assert_true(container.contains(i));
		container.deleteMin();

		assert(container.size() == N - 1 - i);
	}
	assert(container.empty());
	assert_exception_not_thrown(container.deleteMin());

	size_t elems[] = {
		24, 56, 94, 13, 25, 26, 27, 8, 37, 29,
		3, 65, 127, 88, 53, 16, 7, 78, 69, 13
	};

	for (size_t elem : elems)
		container.insert(elem);

	assert(container.size() == 19);

	size_t mins[] = {
		3, 7, 8, 13, 16, 24, 25, 26, 27, 29,
		37, 53, 56, 65, 69, 78, 88, 94, 127
	};

	for (size_t i = 0; i < 19; i++) {
		for (size_t j = i; j < 19; j++) {
			assert_true(container.contains(mins[j]));
		}

		container.deleteMin();
		assert_false(container.contains(mins[i]));
	}
	assert(container.empty());
	assert_exception_not_thrown(container.deleteMin());
}

void testInsertAndDeleteMax() {
	const size_t N = 64;
	Set<size_t> container;

	for(size_t i = 0; i < N; i++)
		container.insert(i);

	assert(!container.empty());
	assert(container.size() == N);

	for (size_t i = 0; i < N; i++) {
		assert_true(container.contains(N - 1 - i));
		container.deleteMax();

		assert(container.size() == N - 1 - i);
	}
	assert(container.empty());
	assert_exception_not_thrown(container.deleteMax());

	size_t elems[] = {
		24, 56, 94, 13, 25, 26, 27, 8, 37, 29,
		3, 65, 127, 88, 53, 16, 7, 78, 69, 13
	};
	for (size_t elem : elems)
		container.insert(elem);

	size_t maxs[] = {
		127, 94, 88, 78, 69, 65, 56, 53, 37,
		29, 27, 26, 25, 24, 16, 13, 8, 7, 3
	};

	for (size_t i = 0; i < 19; i++) {
		for (size_t j = i; j < 19; j++) {
			assert_true(container.contains(maxs[j]));
		}

		container.deleteMax();
		assert_false(container.contains(maxs[i]));
	}
	assert(container.empty());
	assert_exception_not_thrown(container.deleteMax());
}

void testInsertAndDelete() {
	const size_t N = 2048;
	Set<size_t> container;

	for(size_t i = 0; i < N; i++)
		container.insert(i);

	assert(!container.empty());
	assert(container.size() == N);

	for (size_t i = 0; i < N; i++) {
		assert_true(container.contains(i));
		container.erase(i);

		assert(container.size() == N - 1 - i);
	}
	assert(container.empty());
}

void testInsertAndDeleteArbitrary() {
	Set<size_t> container;

	size_t elems[] = {
		24, 56, 94, 13, 25, 26, 27, 8, 37, 29,
		3, 65, 127, 88, 53, 16, 7, 78, 69, 13
	};
	for (size_t elem : elems)
		container.insert(elem);

	size_t indexesToErase[] = {
		3, 5, 7, 10, 4, 12, 18, 9, 1,
		8, 6, 11, 17, 14, 15, 2, 0, 13, 16
	};

	for (size_t i = 0; i < 19; i++) {
		for (size_t j = i; j < 19; j++)
			assert_true(container.contains(elems[indexesToErase[j]]));

		container.erase(elems[indexesToErase[i]]);
		assert_false(container.contains(elems[indexesToErase[i]]));
	}
	assert(container.empty());
	assert_exception_not_thrown(container.erase(3));
}

void testClear() {
	const size_t N = 24;
	Set<size_t> container;

	for(size_t i = 0; i < N; i++)
		container.insert(i);

	assert(!container.empty());
	assert(container.size() == N);

	container.clear();

	assert(container.empty());
	assert(container.size() == 0);
}

void testSwap() {
	const size_t N = 32;
	Set<size_t> containerA;
	Set<size_t> containerB;

	containerA.insert(0);
	containerB.insert(1);
	containerB.insert(2);
	containerA.swap(containerB);

	assert(containerA.size() == 2);
	assert_true(containerA.contains(1));
	assert_true(containerA.contains(2));

	assert(containerB.size() == 1);
	assert_true(containerB.contains(0));
}

void testCopyCtor() {
	const size_t N = 36;
	Set<size_t> containerA;

	for (size_t i = 0; i < N; i++) containerA.insert(i);

	Set<size_t> containerB(containerA);
	containerA.clear();

	assert(!containerB.full());
	assert(!containerB.empty());
	assert(containerB.size() == N);

	for (size_t i = 0; i < N; i++) assert_true(containerB.contains(i));
}

void testAsignation() {
	const size_t N = 36;
	Set<size_t> containerA;

	for (size_t i = 0; i < N; i++) containerA.insert(i);

	Set<size_t> containerB = containerA;
	containerA.clear();

	assert(!containerB.full());
	assert(!containerB.empty());
	assert(containerB.size() == N);

	for (size_t i = 0; i < N; i++) assert_true(containerB.contains(i));
}

void testPreOrderIterator() {
	Set<size_t> container;
	size_t elems[] = {
		24, 56, 94, 13, 25, 26, 27, 8, 37, 29,
		3, 65, 127, 88, 53, 16, 7, 78, 69, 13
	};

	for (size_t elem : elems)
		container.insert(elem);

	Set<size_t>::PreOrderIterator it = container.prebegin();
	Set<size_t>::PreOrderIterator end = container.preend();

	size_t preOrder[] = {
		56, 26, 24, 8, 7, 3, 16, 13, 25, 29,
		27, 53, 37, 94, 78, 69, 65, 88, 127
	};

	size_t i = 0;
	while(it != end) {
		assert(it.elem() == preOrder[i++]);
		it.next();
	}
}

void testInOrderIterator() {
	Set<size_t> container;
	size_t elems[] = {
		24, 56, 94, 13, 25, 26, 27, 8, 37, 29,
		3, 65, 127, 88, 53, 16, 7, 78, 69, 13
	};

	for (size_t elem : elems)
		container.insert(elem);

	Set<size_t>::InOrderIterator it = container.inbegin();
	Set<size_t>::InOrderIterator end = container.inend();

	size_t inOrder[] = {
		3, 7, 8, 13, 16, 24, 25, 26, 27, 29,
		37, 53, 56, 65, 69, 78, 88, 94, 127
	};

	size_t i = 0;
	while(it != end) {
		assert(it.elem() == inOrder[i++]);
		it.next();
	}
}

void testPostOrderIterator() {
	Set<size_t> container;
	size_t elems[] = {
		24, 56, 94, 13, 25, 26, 27, 8, 37, 29,
		3, 65, 127, 88, 53, 16, 7, 78, 69, 13
	};

	for (size_t elem : elems)
		container.insert(elem);

	Set<size_t>::PostOrderIterator it = container.postbegin();
	Set<size_t>::PostOrderIterator end = container.postend();

	size_t postOrder[] = {
		3, 7, 13, 16, 8, 25, 24, 27, 37, 53,
		29, 26, 65, 69, 88, 78, 127, 94, 56
	};

	size_t i = 0;
	while(it != end) {
		assert(it.elem() == postOrder[i++]);
		it.next();
	}
}

int main() {
	TestEngine::execute("1", &testEmpty);
	TestEngine::execute("2", &testFull);
	TestEngine::execute("3", &testInsertDuplicates);
	TestEngine::execute("4", &testInsertAndDeleteMin);
	TestEngine::execute("5", &testInsertAndDeleteMax);
	TestEngine::execute("6", &testInsertAndDelete);
	TestEngine::execute("7", &testInsertAndDeleteArbitrary);
	TestEngine::execute("8", &testClear);
	TestEngine::execute("9", &testSwap);
	TestEngine::execute("10", &testCopyCtor);
	TestEngine::execute("11", &testAsignation);

	TestEngine::execute("12", &testPreOrderIterator);
	TestEngine::execute("13", &testInOrderIterator);
	TestEngine::execute("14", &testPostOrderIterator);
	TestEngine::printResults();
	return 0;
}
