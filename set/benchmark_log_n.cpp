#include "common/common.hpp"
#include <math.h>

int main() {
	ilc::csv::CsvWriter csv;

	BENCHMARK_MATH_FOO_BEGIN("log_n.csv")
	
	size_t x = i * M;

	double logN = log(i * M);
	if (x == 0) {
		logN = 0;
	}
	logN *= 5000;
	logN += 500000;
	BENCHMARK_MATH_FOO_WRITE(logN, logN)
	BENCHMARK_MATH_FOO_END()
	return 0;
}
