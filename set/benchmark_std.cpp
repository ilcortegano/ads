#include "common/common.hpp"
#include <set>


int main() {
	ilc::csv::CsvWriter csv;
	std::set<int> set;

	BENCHMARK_BEGIN("std_insert.csv")
	{
		set.insert(i * M + j);
	}
	BENCHMARK_STD_CSV_WRITE()
	BENCHMARK_END("insert")

	BENCHMARK_BEGIN("std_erase.csv")
	{
		set.erase(i * M + j);
	}
	BENCHMARK_STD_CSV_WRITE()
	BENCHMARK_END("erase")
	return 0;
}
