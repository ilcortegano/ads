#pragma once
#include <stdexcept>
#include "stack/stack.hpp"

namespace ilc::ads {

	template <class T>
	class Set {

	private:
		static const bool RED = true;
		static const bool BLACK = false;

		class Node {

		public:
			Node(const T &elem) : _elem(elem), _left(nullptr), _right(nullptr), _color(RED) {};
			Node(Node *other) : _elem(other->_elem), _left(nullptr), _right(nullptr), _color(other->_color) {};
			Node(Node *left, const T &elem, Node *right) : _elem(elem), _left(left), _right(right), _color(RED) {};

			T _elem;
			bool _color;
			Node *_left;
			Node *_right;
		};

		class BaseIterator {
		public:
			bool hasNext() const {
				return !_parents.empty();
			}

			void next() {
				if (_parents.empty()) {
					_curr = nullptr;
				}
				else {
					_curr = getNext();
				}
			}

			const T &elem() const {
				if (!_curr) throw std::out_of_range("");
				return _curr->_elem;
			}
			
			bool operator==(const BaseIterator &other) const {
				return _curr == other._curr;
			}
			
			bool operator!=(const BaseIterator &other) const {
				return !(this->operator==(other));
			}

			const T &operator*() const {
				return elem();
			}

			BaseIterator &operator++() {
				next();
				return *this;
			}

		protected:
			friend class Set;

			BaseIterator(Node* curr): _curr(curr) {}

			virtual Node* getNext() = 0;

			Node *_curr;
			Stack<Node *> _parents;
		};

	public:
		//------------------------------------//
		//     CONSTRUCTORS & DESTRUCTOR      //
		//------------------------------------//
		Set(): _root(nullptr), _elems(0) {}

		Set(const Set &other): Set() {
			copy(other);
		}

		~Set() {
			free();
		}

		//------------------------------------//
		//             MODIFIERS              //
		//------------------------------------//

		void insert(const T &elem) {
			if (_elems == SIZE_MAX) throw std::overflow_error("");

			_root = insertElement(_root, elem);
			if (_root)
				_root->_color = BLACK;
		}

		void erase(const T &elem) {
			if (!_root)
				return;

			_root = deleteElement(_root, elem);
		}

		void deleteMax() {
			if (!_root)
				return;

			_root = deleteMax(_root);
		}

		void deleteMin() {
			if (!_root)
				return;

			_root = deleteMin(_root);
		}

		void clear() {
			free();
		}

		void swap(Set& other) {
			Node* root {_root};
			size_t elems {_elems};

			_root = other._root;
			_elems = other._elems;

			other._root = root;
			other._elems = elems;
		}

		//------------------------------------//
		//             OBSERVERS              //
		//------------------------------------//
		bool contains(const T &elem) const {
			Node *curr = _root;

			while (curr && elem != curr->_elem) {
				curr = elem < curr->_elem? curr->_left: curr->_right;
			}
			return curr != nullptr;
		}

		size_t size() const {
			return _elems;
		}

		size_t memorySpace() const {
			return sizeof(Set) + sizeof(Node) * _elems;
		}

		bool empty() const {
			return _elems == 0;
		}

		bool full() const {
			return _elems == SIZE_MAX;
		}

		//------------------------------------//
		//             OPERATORS              //
		//------------------------------------//
		Set &operator=(const Set &other) {
			if (this != &other) {
				Set tmp{other};
				swap(tmp);
			}
			return *this;
		}

		//----------------------------------------------//
		//          CONST-PRE-ORDER-ITERATOR            //
		//----------------------------------------------//
		class PreOrderIterator: public BaseIterator {
		protected:
			friend class Set;

			PreOrderIterator(Node* curr): BaseIterator(curr) {
				if (curr) {
					if (curr->_right) this->_parents.push(curr->_right);
					if (curr->_left) this->_parents.push(curr->_left);
				}
			}

			Node* getNext() {
				Node *node = this->_parents.top();
				this->_parents.pop();

				if (node->_right) this->_parents.push(node->_right);
				if (node->_left) this->_parents.push(node->_left);
				return node;
			}
		};

		PreOrderIterator prebegin() const {
			return PreOrderIterator(_root);
		}

		PreOrderIterator preend() const {
			return PreOrderIterator(nullptr);
		}

		//----------------------------------------------//
		//          CONST-IN-ORDER-ITERATOR            //
		//----------------------------------------------//
		class InOrderIterator: public BaseIterator {
		protected:
			friend class Set;

			InOrderIterator(Node* curr): BaseIterator(curr) {
				if (curr) {
					sinkLeft(curr);
					this->_curr = this->_parents.top();
					this->_parents.pop();
				}
			}

			void sinkLeft(Node *node) {
				while (node) {
					this->_parents.push(node);
					node = node->_left;
				}
			}

			Node* getNext() {
				Node *node = this->_parents.top();
				this->_parents.pop();

				if (node->_right) {
					sinkLeft(node->_right);
					this->_curr = this->_parents.top();
				}
				return node;
			}
		};

		InOrderIterator inbegin() const {
			return InOrderIterator(_root);
		}

		InOrderIterator inend() const {
			return InOrderIterator(nullptr);
		}

		//----------------------------------------------//
		//          CONST-POS-ORDER-ITERATOR            //
		//----------------------------------------------//
		class PostOrderIterator: public BaseIterator {
		protected:
			friend class Set;

			PostOrderIterator(Node* curr): BaseIterator(curr) {
				if (curr) {
					sinkLeft(curr);
					this->_curr = this->_parents.top();
					this->_parents.pop();
				}
			}

			void sinkLeft(Node *node) {
				if (!node)
					return;

				while (node) {
					this->_parents.push(node);
					node = node->_left;
				}
				sinkLeft(this->_parents.top()->_right);
			}

			Node* getNext() {
				if (this->_parents.top()->_right != this->_curr) {
					sinkLeft(this->_parents.top()->_right);
				}

				Node *node = this->_parents.top();
				this->_parents.pop();
				return node;
			}
		};

		PostOrderIterator postbegin() const {
			return PostOrderIterator(_root);
		}

		PostOrderIterator postend() const {
			return PostOrderIterator(nullptr);
		}

	private:
		bool isRed(Node *node) const {
			return node && node->_color;
		}

		bool isLeaf(Node *node) const {
			return node && !node->_left && !node->_right;
		}

		Node* insertElement(Node* node, const T &elem) {

			if (!node) {
				_elems++;
				return new Node(elem);
			} 

			if (isRed(node->_left) && isRed(node->_right))
				colorFlip(node);

			if (node->_elem == elem) {
				// nothing
			} else if (elem < node->_elem) {
				node->_left = insertElement(node->_left, elem);
			} else {
				node->_right = insertElement(node->_right, elem);
			}

			if (isRed(node->_right))
				node = rotateLeft(node);

			if (isRed(node->_left) && isRed(node->_left->_left))
				node = rotateRight(node);

			return node;
		}

		Node* rotateLeft(Node* node) {
			Node *root = node->_right;
			node->_right = root->_left;
			root->_left = node;
			root->_color = root->_left->_color;
			root->_left->_color = RED;
			return root;
		}

		Node* rotateRight(Node* node) {
			Node *root = node->_left;
			node->_left = root->_right;
			root->_right = node;
			root->_color = root->_right->_color;
			root->_right->_color = RED;
			return root;
		}

		Node* colorFlip(Node* node) {
			node->_color = !node->_color;
			node->_left->_color = !node->_left->_color;
			node->_right->_color = !node->_right->_color;
			return node;
		}

		Node* fixUp(Node* node) {

			if (isRed(node->_right))
				node = rotateLeft(node);
			if (isRed(node->_left) && isRed(node->_left->_left))
				node = rotateRight(node);
			if (isRed(node->_left) && isRed(node->_right))
				colorFlip(node);

			return node;
		}

		Node* moveRedRight(Node* node) {
			colorFlip(node);
			if (isRed(node->_left->_left)) {
				node = rotateRight(node);
				colorFlip(node);
			}
			return node;
		}

		Node* moveRedLeft(Node* node) {
			colorFlip(node);
			if (isRed(node->_right->_left)) {
				node->_right = rotateRight(node->_right);
				node = rotateLeft(node);
				colorFlip(node);
			}
			return node;
		}

		Node* deleteMax(Node* node) {
			if (isRed(node->_left))
				node = rotateRight(node);

			if (!node->_right) {
				_elems--;
				delete node;
				return nullptr;
			}

			if (!isRed(node->_right) && !isRed(node->_right->_left))
				node = moveRedRight(node);

			node->_right = deleteMax(node->_right);
			return fixUp(node);
		}

		Node* deleteMin(Node* node) {
			if (!node->_left) {
				_elems--;
				delete node;
				return nullptr;
			}

			if (!isRed(node->_left) && !isRed(node->_left->_left))
				node = moveRedLeft(node);

			node->_left = deleteMin(node->_left);
			return fixUp(node);
		}

		const T &min(Node* node) {
			if (node->_left)
				return min(node->_left);
			return node->_elem;
		}

		Node* deleteElement(Node* node, const T &elem) {

			if (elem < node->_elem) {
				if (!isRed(node->_left) && !isRed(node->_left->_left))
					node = moveRedLeft(node);

				node->_left = deleteElement(node->_left, elem);
			} else {
				if (isRed(node->_left))
					node = rotateRight(node);

				if (elem == node->_elem && !node->_right) {
					_elems--;
					delete node;
					return nullptr;
				}

				if (!isRed(node->_right) && !isRed(node->_right->_left))
					node = moveRedRight(node);

				if (elem == node->_elem) {
					node->_elem = min(node->_right);
					node->_right = deleteMin(node->_right);
				} else {
					node->_right = deleteElement(node->_right, elem);
				}
			}
			return fixUp(node);
		}

		void free() {
			if (!_root)
				return;

			freeNode(_root);
			_root = nullptr;
			_elems = 0;
		}

		void freeNode(Node *node) {

			if (node->_left)
				freeNode(node->_left);
			if (node->_right)
				freeNode(node->_right);

			delete node;
		}

		void copy(const Set &other) {
			_elems = other._elems;
			_root = copyNode(other._root);
		}

		Node* copyNode(Node *node) {
			if (!node)
				return nullptr;

			Node *newNode = new Node(node);
			if (node->_left)
				newNode->_left = copyNode(node->_left);

			if (node->_right)
				newNode->_right = copyNode(node->_right);
			return newNode;
		}

		Node *_root;
		size_t _elems;
	};
};