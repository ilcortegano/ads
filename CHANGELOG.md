# Change log

## Version 1.0.1 - 06 Apr 2023

- Added _ilc::ads::hash_map_ improved memorySpace method.

## Version 1.0.0 - 05 Apr 2023

- Added _ilc::ads::hash_map_ benchmarks against _std::map_.
- Added _ilc::ads::tree_map_ benchmarks against _std::map_.

## Version 0.7.1 - 02 Apr 2023

- Added _ilc::ads::set_ iterators.

## Version 0.7.0 - 31 Mar 2023

- Added _ilc::ads::set_ benchmarks against _std::set_.
- Added _ilc::ads::set_ pre-order iterator.

## Version 0.6.1 - 08 Mar 2023

- General optimizations
- Removed _std_ memory space bar from charts. Reason: the current method is not real as leads to misunderstanding.

## Version 0.6.0 - 07 Mar 2023

- Added _ilc::ads::vector_ benchmarks against _std::vector_.

## Version 0.5.1 - 04 Mar 2023

- General optimizations

## Version 0.5.0 - 27 Feb 2023

- Added _ilc::ads::list_ benchmarks against _std::list_.
- Added _ilc::ads::list_ iterators.

## Version 0.4.0 - 22 Feb 2023

- Added _ilc::ads::map_ benchmarks against _std::map_.
- Added _ilc::ads::map_ const iterator.
- Added _memorySpace_ method for all data structures.
- Improved commons folder.

## Version 0.3.0 - 14 Feb 2023

- Added _ilc::ads::heap_ benchmarks against _std::priority_queue_.

## Version 0.2.3 - 14 Feb 2023

- Improved  _ilc::ads::queue_.
- Improved  _ilc::ads::stack_.

## Version 0.2.2 - 06 Feb 2023

- Added _ilc::ads::queue_ benchmarks against _std::queue_.

## Version 0.1.2 - 06 Feb 2023

- Added benchmark utilities to _common_ folder.
- Added test utilities.
- Added new methods for _stack_ container.
- Added tests for _stack_ container.

## Version 0.1.1 - 15 Jan 2023

- Added _Python_ script for data analysis.
- Fixed _ilc::ads::stack_ memory space metric. Now uses the `capacity` method instead of the `size` method.

## Version 0.1.0 - 14 Jan 2023

- Added _ilc::ads::stack_ benchmarks against _std::stack_.

## Version 0.0.0 - 14 Jan 2023

- Initial commit.
