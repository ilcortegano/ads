#include "common/common.hpp"
#include "stack/stack.hpp"


int main() {
	ilc::csv::CsvWriter csv;
	ilc::ads::Stack<int> stack;

	BENCHMARK_BEGIN("isauro_push.csv")
	{
		stack.push(0);
	}
	BENCHMARK_CSV_WRITE() << stack.memorySpace() << "\n";
	BENCHMARK_END("push")

	BENCHMARK_BEGIN("isauro_pop.csv")
	{
		stack.pop();
	}
	BENCHMARK_CSV_WRITE() << stack.memorySpace() << "\n";
	BENCHMARK_END("pop")
	return 0;
}
