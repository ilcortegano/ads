#include "common/common.hpp"
#include <stack>


int main() {
	const size_t ADS_SIZE = sizeof(std::stack<int>);
	ilc::csv::CsvWriter csv;
	std::stack<int> stack;

	BENCHMARK_BEGIN("std_push.csv")
	{
		stack.push(0);
	}
	BENCHMARK_STD_CSV_WRITE()
	BENCHMARK_END("push")

	BENCHMARK_BEGIN("std_pop.csv")
	{
		stack.pop();
	}
	BENCHMARK_STD_CSV_WRITE()
	BENCHMARK_END("pop")
	return 0;
}
