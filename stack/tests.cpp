#include "common/testing/engine.hpp"
#include "common/testing/asserts.hpp"
#include "stack/stack.hpp"
using namespace ilc::test;
using namespace ilc::ads;

template<typename T>
struct FakeStack {
	T* array;
	size_t elems;
	size_t capacity;
};


void testEmpty() {
	Stack<size_t> container;

	assert(container.empty());
	assert(!container.full());
	assert(container.size() == 0);
	assert(container.capacity() == INITIAL_CAPACITY);
	assert_exception_thrown(container.top());

	container.pop();
	assert(container.size() == 0);
}

void testFull() {
	Stack<size_t> container;
	FakeStack<size_t>* manipulator = reinterpret_cast<FakeStack<size_t>*>(&container);
	manipulator->elems = MAX_CAPACITY;

	assert(!container.empty());
	assert(container.full());
	assert(container.size() == MAX_CAPACITY);
	assert_exception_thrown(container.push(0));
}

void testPushAndPop() {
	const size_t N = INITIAL_CAPACITY + 1;
	Stack<size_t> container;

	for(size_t i = 0; i < N; i++) {
		container.push(i);
		assert(container.top() == i);
	}
	assert(!container.empty());
	assert(container.size() == N);
	assert(container.capacity() == INITIAL_CAPACITY * 2);

	for(size_t i = 1; i <= N; i++) {
		assert(container.top() == N - i);
		container.pop();
	}
	assert(container.empty());
	assert(container.size() == 0);
	assert(container.capacity() == 2);
}

void testFit() {
	const size_t N = INITIAL_CAPACITY + 1;
	Stack<size_t> container;

	for(size_t i = 0; i < N; i++) {
		container.push(i);
	}
	container.fit();

	assert(container.size() == N);
	assert(container.capacity() == N);
}

void testClear() {
	const size_t N = INITIAL_CAPACITY + 1;
	Stack<size_t> container;

	for(size_t i = 0; i < N; i++) {
		container.push(i);
	}
	container.clear();

	assert(container.size() == 0);
	assert(container.capacity() == (INITIAL_CAPACITY * 2));
}

void testSwap() {
	const size_t N = 256;
	Stack<size_t> containerA;
	Stack<size_t> containerB;
	size_t sizeA = rand() % N;
	size_t sizeB = rand() % N;

	for (size_t i = 0; i < sizeA; i++) { containerA.push(i); }
	for (size_t i = 0; i < sizeB; i++) { containerB.push(sizeB - i); }
	containerA.swap(containerB);

	assert(containerA.size() == sizeB);
	assert(containerA.top() ==  1);
	assert(containerB.size() == sizeA);
	assert(containerB.top() == sizeA - 1);
}

void testCopyCtor() {
	const size_t N = INITIAL_CAPACITY + 1;
	Stack<size_t> containerA;

	for (size_t i = 0; i < N; i++) {
		containerA.push(i);
	}

	Stack<size_t> containerB(containerA);
	assert(!containerB.full());
	assert(!containerB.empty());
	assert(containerB.size() == N);
	assert(containerB.capacity() == INITIAL_CAPACITY * 2);

	for (size_t i = 0; i < N; i++) {
		assert(containerB.top() == N - 1 - i);
		containerB.pop();
	}
}

void testAsignation() {
	const size_t N = INITIAL_CAPACITY + 1;
	Stack<size_t> containerA;

	for (size_t i = 0; i < N; i++) {
		containerA.push(i);
	}
	containerA = containerA;
	Stack<size_t> containerB = containerA;

	assert(!containerB.full());
	assert(!containerB.empty());
	assert(containerB.size() == N);
	assert(containerB.capacity() == INITIAL_CAPACITY * 2);

	for (size_t i = 0; i < N; i++) {
		assert(containerB.top() == containerA.top());
		containerB.pop();
		containerA.pop();
	}
}

void testEquals() {
	Stack<size_t> containerA, containerB;

	assert(containerA == containerB);
	assert(!(containerA != containerB));

	containerA.push(0);
	assert(containerA != containerB);

	containerB.push(0);
	assert(containerA == containerB);

	containerA.push(1);
	assert(containerA != containerB);

	containerB.pop();
	containerB.push(0);
	containerB.push(1);
	assert(containerA == containerB);
}

void testAlphanumericOrder() {
	Stack<size_t> containerA, containerB;

	assert(containerA >= containerB);
	assert(containerA <= containerB);
	assert(!(containerA < containerB));
	assert(!(containerA > containerB));

	containerA.push(0);
	assert(containerA > containerB);

	containerB.push(1);
	assert(containerB > containerA);

	containerA.pop();
	containerA.push(0);
	containerA.push(1);
	assert(containerA > containerB);

	containerB.clear();
	containerB.push(0);
	containerB.push(1);
	assert(!(containerA > containerB));
	assert(!(containerA < containerB));
	assert(containerA == containerB);
}

int main() {
	TestEngine::execute("1", &testEmpty);
	TestEngine::execute("2", &testFull);
	TestEngine::execute("3", &testPushAndPop);
	TestEngine::execute("4", &testFit);
	TestEngine::execute("5", &testClear);
	TestEngine::execute("6", &testSwap);
	TestEngine::execute("7", &testCopyCtor);
	TestEngine::execute("8", &testAsignation);
	TestEngine::execute("9", &testEquals);
	TestEngine::execute("10", &testAlphanumericOrder);
	TestEngine::printResults();
	return 0;
}
