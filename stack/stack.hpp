#pragma once
#include <stdexcept>

namespace ilc::ads {

	constexpr size_t INITIAL_CAPACITY = 8;
	constexpr size_t MAX_CAPACITY = 9223372036854775808ULL;


	template <class T>
	class Stack {

	public:
		//------------------------------------//
		//     CONSTRUCTORS & DESTRUCTOR      //
		//------------------------------------//
		Stack(): _array(new T[INITIAL_CAPACITY]), _capacity(INITIAL_CAPACITY), _elems(0) {}

		Stack(const Stack &other) {
			copy(other);
		}

		~Stack() {
			delete[] _array;
		}

		//------------------------------------//
		//             MODIFIERS              //
		//------------------------------------//
		void push(const T &elem) {
			if (_elems == MAX_CAPACITY) throw std::overflow_error("");
			if (_elems == _capacity)
				resize(_capacity << 1);

			_array[_elems] = elem;
			_elems++;
		}

		void pop() {
			_elems = _elems - (_elems != 0);

			if (_elems < (_capacity >>  2))
				resize(_capacity >> 1);
		}

		void fit() {
			if (_elems == 0)
				return;

			resize(_elems);
		}

		void clear() {
			_elems = 0;
		}

		void swap(Stack& other) {
			T* array {_array};
			size_t capacity {_capacity};
			size_t elems {_elems};

			_array = other._array;
			_capacity = other._capacity;
			_elems = other._elems;

			other._array = array;
			other._capacity = capacity;
			other._elems = elems;
		}

		//------------------------------------//
		//             OBSERVERS              //
		//------------------------------------//
		const T &top() const {
			if (_elems == 0) throw std::out_of_range("");
			return _array[_elems - 1];
		}

		size_t size() const {
			return _elems;
		}

		size_t capacity() const {
			return _capacity;
		}

		size_t memorySpace() const {
			return sizeof(Stack) + sizeof(T) * _capacity;
		}

		bool empty() const {
			return _elems == 0;
		}

		bool full() const {
			return _elems == MAX_CAPACITY;
		}

		bool operator==(const Stack &other) const {
			size_t i = 0;

			if (_elems != other._elems)
				return false;

			while ((i != _elems) && (_array[i] == other._array[i]))
				i++;

			return (i == _elems);
		}

		bool operator!=(const Stack &other) const {
			return !(*this == other);
		}

		bool operator<(const Stack &other) const {
			size_t i = 0;
			size_t p1 = _elems - 1;
			size_t p2 = other._elems - 1;

			while (i < _elems) {
				if (other._elems <= i || other._array[p2] < _array[p1]) return false;
				else if (_array[p1] < other._array[p2]) return true;
				i++;
				p1--;
				p2--;
			}
			return (other._elems > i);
		}

		bool operator>(const Stack &other) const {
			return other < *this;
		}

		bool operator<=(const Stack &other) const {
			return !(other < *this);
		}

		bool operator>=(const Stack &other) const {
			return !(*this < other);
		}

		//------------------------------------//
		//             OPERATORS              //
		//------------------------------------//
		Stack &operator=(const Stack &other) {
			if (this != &other) {
				Stack tmp{other};
				swap(tmp);
			}
			return *this;
		}

	private:
		T *_array;
		size_t _elems;
		size_t _capacity;

		void resize(size_t size) {
			T *old = _array;
			_capacity = size;
			_array = new T[size];

			memcpy(_array, old, _elems * sizeof(T));
			delete[] old;
		}

		void copy(const Stack &other) {
			_elems = other._elems;
			_capacity = other._capacity;

			_array = new T[other._capacity];
			memcpy(_array, other._array, other._elems * sizeof(T));
		}
	};
};