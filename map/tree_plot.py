import sys
import os

current = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.dirname(current))

from common import plot, Config

config = Config()
config.BASE_PATH = current
config.ADS = 'map'
config.STD_ADS = config.ADS
config.MATH_FN = 'O(log_n+a)'
config.MATH_FN2 = 'O(log_n+b)'
config.OPERATION = 'tree_insert'
config.STD_OPERATION = 'insert'
plot(config)

config.MATH_FN = None
config.MATH_FN2 = None
config.OPERATION = 'tree_erase'
config.STD_OPERATION = 'erase'
plot(config)