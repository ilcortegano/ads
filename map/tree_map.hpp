#pragma once
#include <stdexcept>
#include "stack/stack.hpp"

namespace ilc::ads {

	template <class K, class V>
	class Map {

	private:
		static const bool RED = true;
		static const bool BLACK = false;

		class Node {

		public:
			Node(const K &key, const V &value) : _key(key), _elem(value), _left(nullptr), _right(nullptr), _color(RED) {};
			Node(Node *other) : _key(other->_key), _elem(other->_elem), _left(nullptr), _right(nullptr), _color(other->_color) {};
			Node(Node *left, const K &key, const V &value, Node *right) : _key(key), _elem(value), _left(left), _right(right), _color(RED) {};

			K _key;
			V _elem;
			bool _color;
			Node *_left;
			Node *_right;
		};

		class BaseIterator {
		public:
			bool hasNext() const {
				return !_parents.empty();
			}

			void next() {
				if (_parents.empty()) {
					_curr = nullptr;
				}
				else {
					_curr = getNext();
				}
			}

			const K &key() const {
				if (!_curr) throw std::out_of_range("");
				return _curr->_key;
			}

			const V &value() const {
				if (!_curr) throw std::out_of_range("");
				return _curr->_elem;
			}
			
			bool operator==(const BaseIterator &other) const {
				return _curr == other._curr;
			}
			
			bool operator!=(const BaseIterator &other) const {
				return !(this->operator==(other));
			}

			const V &operator*() const {
				return value();
			}

			BaseIterator &operator++() {
				next();
				return *this;
			}

		protected:
			friend class Map;

			BaseIterator(Node* curr): _curr(curr) {}

			virtual Node* getNext() = 0;

			Node *_curr;
			Stack<Node *> _parents;
		};

	public:
		//------------------------------------//
		//     CONSTRUCTORS & DESTRUCTOR      //
		//------------------------------------//
		Map(): _root(nullptr), _elems(0) {}

		Map(const Map &other): Map() {
			copy(other);
		}

		~Map() {
			free();
		}

		//------------------------------------//
		//             MODIFIERS              //
		//------------------------------------//

		void insert(const K &key, const V &value) {
			if (_elems == SIZE_MAX) throw std::overflow_error("");

			_root = insertElement(_root, key, value);
		}

		void erase(const K &key) {
			if (!_root)
				return;

			_root = deleteElement(_root, key);
		}

		void clear() {
			free();
		}

		void swap(Map& other) {
			Node* root {_root};
			size_t elems {_elems};

			_root = other._root;
			_elems = other._elems;

			other._root = root;
			other._elems = elems;
		}

		//------------------------------------//
		//             OBSERVERS              //
		//------------------------------------//
		bool contains(const K &key) const {
			Node *curr = _root;

			while (curr && key != curr->_key) {
				curr = key < curr->_key? curr->_left: curr->_right;
			}
			return curr != nullptr;
		}

		size_t size() const {
			return _elems;
		}

		size_t memorySpace() const {
			return sizeof(Map) + sizeof(Node) * _elems;
		}

		bool empty() const {
			return _elems == 0;
		}

		bool full() const {
			return _elems == SIZE_MAX;
		}

		//------------------------------------//
		//             OPERATORS              //
		//------------------------------------//
		Map &operator=(const Map &other) {
			if (this != &other) {
				Map tmp{other};
				swap(tmp);
			}
			return *this;
		}

		V &operator[](const K &key) {
			Node *curr = _root;

			while (curr && key != curr->_key) {
				curr = key < curr->_key? curr->_left: curr->_right;
			}
			if (!curr)
				throw std::out_of_range("");
			return curr->_elem;
		}

		//----------------------------------------------//
		//          CONST-PRE-ORDER-ITERATOR            //
		//----------------------------------------------//
		class PreOrderIterator: public BaseIterator {
		protected:
			friend class Map;

			PreOrderIterator(Node* curr): BaseIterator(curr) {
				if (curr) {
					if (curr->_right) this->_parents.push(curr->_right);
					if (curr->_left) this->_parents.push(curr->_left);
				}
			}

			Node* getNext() {
				Node *node = this->_parents.top();
				this->_parents.pop();

				if (node->_right) this->_parents.push(node->_right);
				if (node->_left) this->_parents.push(node->_left);
				return node;
			}
		};

		PreOrderIterator prebegin() const {
			return PreOrderIterator(_root);
		}

		PreOrderIterator preend() const {
			return PreOrderIterator(nullptr);
		}

		//----------------------------------------------//
		//          CONST-IN-ORDER-ITERATOR            //
		//----------------------------------------------//
		class InOrderIterator: public BaseIterator {
		protected:
			friend class Map;

			InOrderIterator(Node* curr): BaseIterator(curr) {
				if (curr) {
					sinkLeft(curr);
					this->_curr = this->_parents.top();
					this->_parents.pop();
				}
			}

			void sinkLeft(Node *node) {
				while (node) {
					this->_parents.push(node);
					node = node->_left;
				}
			}

			Node* getNext() {
				Node *node = this->_parents.top();
				this->_parents.pop();

				if (node->_right) {
					sinkLeft(node->_right);
					this->_curr = this->_parents.top();
				}
				return node;
			}
		};

		InOrderIterator inbegin() const {
			return InOrderIterator(_root);
		}

		InOrderIterator inend() const {
			return InOrderIterator(nullptr);
		}

		//----------------------------------------------//
		//          CONST-POS-ORDER-ITERATOR            //
		//----------------------------------------------//
		class PostOrderIterator: public BaseIterator {
		protected:
			friend class Map;

			PostOrderIterator(Node* curr): BaseIterator(curr) {
				if (curr) {
					sinkLeft(curr);
					this->_curr = this->_parents.top();
					this->_parents.pop();
				}
			}

			void sinkLeft(Node *node) {
				if (!node)
					return;

				while (node) {
					this->_parents.push(node);
					node = node->_left;
				}
				sinkLeft(this->_parents.top()->_right);
			}

			Node* getNext() {
				if (this->_parents.top()->_right != this->_curr) {
					sinkLeft(this->_parents.top()->_right);
				}

				Node *node = this->_parents.top();
				this->_parents.pop();
				return node;
			}
		};

		PostOrderIterator postbegin() const {
			return PostOrderIterator(_root);
		}

		PostOrderIterator postend() const {
			return PostOrderIterator(nullptr);
		}

	private:
		bool isRed(Node *node) const {
			return node && node->_color;
		}

		bool isLeaf(Node *node) const {
			return node && !node->_left && !node->_right;
		}

		Node* insertElement(Node* node, const K &key, const V &value) {

			if (!node) {
				_elems++;
				return new Node(key, value);
			} 

			if (isRed(node->_left) && isRed(node->_right))
				colorFlip(node);

			if (node->_key == key) {
				node->_elem = value;
			} else if (key < node->_key) {
				node->_left = insertElement(node->_left, key, value);
			} else {
				node->_right = insertElement(node->_right, key, value);
			}

			if (isRed(node->_right))
				node = rotateLeft(node);

			if (isRed(node->_left) && isRed(node->_left->_left))
				node = rotateRight(node);

			return node;
		}

		Node* rotateLeft(Node* node) {
			Node *root = node->_right;
			node->_right = root->_left;
			root->_left = node;
			root->_color = root->_left->_color;
			root->_left->_color = RED;
			return root;
		}

		Node* rotateRight(Node* node) {
			Node *root = node->_left;
			node->_left = root->_right;
			root->_right = node;
			root->_color = root->_right->_color;
			root->_right->_color = RED;
			return root;
		}

		Node* colorFlip(Node* node) {
			node->_color = !node->_color;
			node->_left->_color = !node->_left->_color;
			node->_right->_color = !node->_right->_color;
			return node;
		}

		Node* fixUp(Node* node) {

			if (isRed(node->_right))
				node = rotateLeft(node);
			if (isRed(node->_left) && isRed(node->_left->_left))
				node = rotateRight(node);
			if (isRed(node->_left) && isRed(node->_right))
				colorFlip(node);

			return node;
		}

		Node* moveRedRight(Node* node) {
			colorFlip(node);
			if (isRed(node->_left->_left)) {
				node = rotateRight(node);
				colorFlip(node);
			}
			return node;
		}

		Node* moveRedLeft(Node* node) {
			colorFlip(node);
			if (isRed(node->_right->_left)) {
				node->_right = rotateRight(node->_right);
				node = rotateLeft(node);
				colorFlip(node);
			}
			return node;
		}

		Node* deleteMin(Node* node) {
			if (!node->_left) {
				_elems--;
				delete node;
				return nullptr;
			}

			if (!isRed(node->_left) && !isRed(node->_left->_left))
				node = moveRedLeft(node);

			node->_left = deleteMin(node->_left);
			return fixUp(node);
		}

		Node* minNode(Node* node) {
			if (node->_left)
				return minNode(node->_left);
			return node;
		}

		Node* deleteElement(Node* node, const K &key) {

			if (key < node->_key) {
				if (!isRed(node->_left) && !isRed(node->_left->_left))
					node = moveRedLeft(node);

				node->_left = deleteElement(node->_left, key);
			} else {
				if (isRed(node->_left))
					node = rotateRight(node);

				if (key == node->_key && !node->_right) {
					_elems--;
					delete node;
					return nullptr;
				}

				if (!isRed(node->_right) && !isRed(node->_right->_left))
					node = moveRedRight(node);

				if (key == node->_key) {
					Node *min = minNode(node->_right);

					node->_key = min->_key;
					node->_elem = min->_elem;
					node->_right = deleteMin(node->_right);
				} else {
					node->_right = deleteElement(node->_right, key);
				}
			}
			return fixUp(node);
		}

		void free() {
			if (!_root)
				return;

			freeNode(_root);
			_root = nullptr;
			_elems = 0;
		}

		void freeNode(Node *node) {

			if (node->_left)
				freeNode(node->_left);
			if (node->_right)
				freeNode(node->_right);

			delete node;
		}

		void copy(const Map &other) {
			_elems = other._elems;
			_root = copyNode(other._root);
		}

		Node* copyNode(Node *node) {
			if (!node)
				return nullptr;

			Node *newNode = new Node(node);
			if (node->_left)
				newNode->_left = copyNode(node->_left);

			if (node->_right)
				newNode->_right = copyNode(node->_right);
			return newNode;
		}

		Node *_root;
		size_t _elems;
	};
};