#include "common/common.hpp"
#include <map>


int main() {
	const size_t ADS_SIZE = sizeof(std::map<size_t, size_t>);
	ilc::csv::CsvWriter csv;
	std::map<size_t, size_t> map;

	BENCHMARK_BEGIN("std_insert.csv")
	{
		map[i * M + j] = i;
	}
	BENCHMARK_STD_CSV_WRITE()
	BENCHMARK_END("insert")

	BENCHMARK_BEGIN("std_erase.csv")
	{
		map.erase(i * M + j);
	}
	BENCHMARK_STD_CSV_WRITE()
	BENCHMARK_END("erase")
	return 0;
}
