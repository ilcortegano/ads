#pragma once
#include <stdexcept>
#include "common/hash.hpp"

namespace ilc::ads {

	constexpr size_t INITIAL_CAPACITY = 8;
	constexpr size_t POOL_CAPACITY = 2;
	constexpr size_t MAX_CAPACITY = 9223372036854775808ULL;
	constexpr float MIN_OCUPANCY = 0.25f;
	constexpr float MAX_OCUPANCY = 0.80f;

	template <class K, class V>
	class Map {

	private:
		class Pair {

		public:
			Pair(): _key(K()), _value(V()) {};

			Pair(const K &key, const V &value): _key(key), _value(value) {};

			Pair(const Pair &other): _key(other._key), _value(other._value) {};

			K _key;
			V _value;
		};

		class PairPool {

		public:
			PairPool(): _pool(new Pair[POOL_CAPACITY]), _elems(0), _capacity(POOL_CAPACITY) {};

			PairPool(const PairPool &other):
				_pool(new Pair[other._capacity]),
				_elems(other._elems),
				_capacity(other._capacity) {

				memcpy(_pool, other._pool, other._elems * sizeof(Pair));
			};

			~PairPool() {
				delete[] _pool;
			}

			Pair *_pool;
			size_t _elems;
			size_t _capacity;

			bool insert(const K &key, const V &value) {
				if (_elems == POOL_CAPACITY)
					resize(_capacity << 4);

				size_t index = search(key);

				_pool[index]._key = key;
				_pool[index]._value = value;

				return index == _elems++;
			}

			bool erase(const K &key) {
				size_t index = search(key);

				for (size_t i = index; i < _elems - 1; ++i)
					_pool[i] = _pool[i + 1];
				return index != _elems--;
			}

			const V &at(const K &key) const {
				size_t index = search(key);
				return _pool[index]._value;
			}

			bool contains(const K &key) const {
				size_t index = search(key);
				
				return index != _elems;
			}

			size_t search(const K &key) const {
				size_t index = 0;

				while (index <_elems && _pool[index]._key != key)
					++index;
				return index;
			}

			size_t memorySpace() const {
				return sizeof(PairPool) + sizeof(Pair) * _capacity;
			}

		private:
			void resize(size_t size) {
				Pair *old = _pool;
				_capacity = size;
				_pool = new Pair[size];

				memcpy(_pool, old, _elems * sizeof(Pair));
				delete[] old;
			}
		}; 

	public:
		//------------------------------------//
		//     CONSTRUCTORS & DESTRUCTOR      //
		//------------------------------------//
		Map(): _pools(new PairPool*[INITIAL_CAPACITY]), _capacity(INITIAL_CAPACITY), _elems(0) {

			for (size_t i = 0; i < _capacity; ++i) _pools[i] = nullptr;
		}

		Map(const Map &other) {
			copy(other);
		}

		~Map() {
			free();
		}

		//------------------------------------//
		//             MODIFIERS              //
		//------------------------------------//
		void insert(const K &key, const V &value) {
			if (_elems == MAX_CAPACITY) throw std::overflow_error("");

			float occupancy = ((float) _elems) / _capacity;
			if (occupancy > MAX_OCUPANCY)
				resize(_capacity << 1);

			size_t index = haskKey(key);

			if (!_pools[index])
				_pools[index] = new PairPool();

			_elems += _pools[index]->insert(key, value);
		}

		void erase(const K &key) {
			size_t index = haskKey(key);

			if (_pools[index]) {
				_elems -= _pools[index]->erase(key);
			}
		}

		void shrink() {
			if (_elems == 0)
				return;

			float occupancy = ((float) _elems) / _capacity;
			if (occupancy < MIN_OCUPANCY)
				resize(_capacity >> 2);
		}

		void clear() {
			freePools();
			_elems = 0;
		}

		void swap(Map& other) {
			PairPool **array {_pools};
			size_t capacity {_capacity};
			size_t elems {_elems};

			_pools = other._pools;
			_capacity = other._capacity;
			_elems = other._elems;

			other._pools = array;
			other._capacity = capacity;
			other._elems = elems;
		}

		//------------------------------------//
		//             OBSERVERS              //
		//------------------------------------//
		bool contains(const K &key) const {
			size_t index = haskKey(key);
			return _pools[index]? _pools[index]->contains(key): false;
		}

		const V &at(const K &key) const {
			size_t index = haskKey(key);

			if (_pools[index]) {
				return _pools[index]->at(key);
			}
			throw std::runtime_error("");
		}

		size_t size() const {
			return _elems;
		}

		size_t capacity() const {
			return _capacity;
		}

		size_t memorySpace() const {
			size_t space = sizeof(Map);

			for (size_t i = 0; i < _capacity; ++i) {
				if (_pools[i])
					space += _pools[i]->memorySpace();
			}
			return space;
		}

		float occupancy() const {
			return ((float) _elems) / _capacity;
		}

		bool empty() const {
			return _elems == 0;
		}

		bool full() const {
			return _elems == MAX_CAPACITY;
		}

		//------------------------------------//
		//             OPERATORS              //
		//------------------------------------//
		Map &operator=(const Map &other) {
			if (this != &other) {
				Map tmp{other};
				swap(tmp);
			}
			return *this;
		}

		V &operator[](const K &key) {
			size_t index = haskKey(key);

			if (!_pools[index]) {
				insert(key, V());
			}

			size_t poolIndex = _pools[index]->search(key);
			return _pools[index]->_pool[poolIndex]._value;
		}

	private:
		PairPool **_pools;
		size_t _elems;
		size_t _capacity;

		inline size_t haskKey(const K &key) const {
			size_t hashCode = ::hash(key);

			while (hashCode >= _capacity)
				hashCode -= _capacity;

			return hashCode;
		}

		void free() {
			freePools();
			delete[] _pools;
		}

		void freePools() {
			for (size_t i = 0; i < _elems; ++i) {
				delete _pools[i];
				_pools[i] = nullptr;
			}
		}

		void resize(size_t size) {
			PairPool **old = _pools;
			size_t oldCapacity = _capacity;

			_capacity = size;
			_pools = new PairPool*[size];
			for (size_t i = 0; i < _capacity; ++i)
				_pools[i] = nullptr;

			for (size_t i = 0; i < oldCapacity; ++i) {
				size_t k = 0;

				while (old[i] && k < old[i]->_elems) {
					Pair p = old[i]->_pool[k];

					size_t index = haskKey(p._key);
					if (!_pools[index])
						_pools[index] = new PairPool();

					_pools[index]->insert(p._key, p._value);
					++k;
				}
			}
			delete[] old;
		}

		void copy(const Map &other) {
			_elems = other._elems;
			_capacity = other._capacity;
			_pools = new PairPool*[other._capacity];

			for (size_t i = 0; i < other._capacity; ++i)
				_pools[i] = other._pools[i]? new PairPool(*other._pools[i]): nullptr;
		}
	};
};