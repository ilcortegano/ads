#include "common/common.hpp"
#include "map/tree_map.hpp"


int main() {
	ilc::csv::CsvWriter csv;
	ilc::ads::Map<size_t, size_t> map;

	BENCHMARK_BEGIN("isauro_tree_insert.csv")
	{
		map.insert(i * M + j, i);
	}
	BENCHMARK_CSV_WRITE() << map.memorySpace() << "\n";
	BENCHMARK_END("insert")

	BENCHMARK_BEGIN("isauro_tree_erase.csv")
	{
		map.erase(i * M + j);
	}
	BENCHMARK_CSV_WRITE() << map.memorySpace() << "\n";
	BENCHMARK_END("erase")
	return 0;
}
