#include "common/testing/engine.hpp"
#include "common/testing/asserts.hpp"
#include "map/tree_map.hpp"
using namespace ilc::test;
using namespace ilc::ads;


template<class K, class V>
class FakeMap {

public:

	struct FakeMapNode {
		K key;
		V elem;
		bool color;
		FakeMapNode *left;
		FakeMapNode *right;
	};

	FakeMapNode *root;
	size_t elems;
};


void testEmpty() {
	Map<size_t, size_t> container;

	assert(container.empty());
	assert(!container.full());
	assert(container.size() == 0);

	assert_false(container.contains(3));
	assert_exception_not_thrown(container.erase(3));
}

void testFull() {
	Map<size_t, size_t> container;
	FakeMap<size_t, size_t>* manipulator = reinterpret_cast<FakeMap<size_t, size_t>*>(&container);
	manipulator->elems = SIZE_MAX;

	assert(!container.empty());
	assert(container.full());
	assert(container.size() == SIZE_MAX);

	assert_exception_thrown(container.insert(3, 3));
}

void testInsertDuplicates() {
	const size_t N = 2048;
	Map<size_t, size_t> container;

	for(size_t i = 0; i < N; i++)
		container.insert(0, 0);

	assert(!container.empty());
	assert(container.size() == 1);
	assert_true(container.contains(0));
}

void testInsertAndDelete() {
	const size_t N = 2048;
	Map<size_t, size_t> container;

	for(size_t i = 0; i < N; i++)
		container.insert(i, i);

	assert(!container.empty());
	assert(container.size() == N);

	for (size_t i = 0; i < N; i++) {
		assert_true(container.contains(i));
		container.erase(i);

		assert(container.size() == N - 1 - i);
	}
	assert(container.empty());

	container.insert(1, 15);
	container.insert(1, 22);
	assert_true(container.contains(1));

	assert_exception_not_thrown(container[1]);
	assert(22 == container[1]);
	assert_exception_thrown(container[2]);	
}

void testInsertAndDeleteArbitrary() {
	Map<size_t, size_t> container;

	size_t elems[] = {
		24, 56, 94, 13, 25, 26, 27, 8, 37, 29,
		3, 65, 127, 88, 53, 16, 7, 78, 69, 13
	};
	for (size_t elem : elems)
		container.insert(elem, elem);

	size_t indexesToErase[] = {
		3, 5, 7, 10, 4, 12, 18, 9, 1,
		8, 6, 11, 17, 14, 15, 2, 0, 13, 16
	};

	for (size_t i = 0; i < 19; i++) {
		for (size_t j = i; j < 19; j++)
			assert_true(container.contains(elems[indexesToErase[j]]));

		container.erase(elems[indexesToErase[i]]);
		assert_false(container.contains(elems[indexesToErase[i]]));
	}
	assert(container.empty());
	assert_exception_not_thrown(container.erase(3));
}

void testClear() {
	const size_t N = 24;
	Map<size_t, size_t> container;

	for(size_t i = 0; i < N; i++)
		container.insert(i, i);

	assert(!container.empty());
	assert(container.size() == N);

	container.clear();

	assert(container.empty());
	assert(container.size() == 0);
}

void testSwap() {
	const size_t N = 32;
	Map<size_t, size_t> containerA;
	Map<size_t, size_t> containerB;

	containerA.insert(0, 0);
	containerB.insert(1, 1);
	containerB.insert(2, 2);
	containerA.swap(containerB);

	assert(containerA.size() == 2);
	assert_true(containerA.contains(1));
	assert_true(containerA.contains(2));

	assert(containerB.size() == 1);
	assert_true(containerB.contains(0));
}

void testCopyCtor() {
	const size_t N = 36;
	Map<size_t, size_t> containerA;

	for (size_t i = 0; i < N; i++) containerA.insert(i, i);

	Map<size_t, size_t> containerB(containerA);
	containerA.clear();

	assert(!containerB.full());
	assert(!containerB.empty());
	assert(containerB.size() == N);

	for (size_t i = 0; i < N; i++) assert_true(containerB.contains(i));
}

void testAsignation() {
	const size_t N = 36;
	Map<size_t, size_t> containerA;

	for (size_t i = 0; i < N; i++) containerA.insert(i, i);

	Map<size_t, size_t> containerB = containerA;
	containerA.clear();

	assert(!containerB.full());
	assert(!containerB.empty());
	assert(containerB.size() == N);

	for (size_t i = 0; i < N; i++) assert_true(containerB.contains(i));
}

void testPreOrderIterator() {
	Map<size_t, size_t> container;
	size_t elems[] = {
		24, 56, 94, 13, 25, 26, 27, 8, 37, 29,
		3, 65, 127, 88, 53, 16, 7, 78, 69, 13
	};

	for (size_t elem : elems)
		container.insert(elem, elem);

	Map<size_t, size_t>::PreOrderIterator it = container.prebegin();
	Map<size_t, size_t>::PreOrderIterator end = container.preend();

	size_t preOrder[] = {
		56, 26, 24, 8, 7, 3, 16, 13, 25, 29,
		27, 53, 37, 94, 78, 69, 65, 88, 127
	};

	size_t i = 0;
	while(it != end) {
		assert(it.value() == preOrder[i++]);
		it.next();
	}
}

void testInOrderIterator() {
	Map<size_t, size_t> container;
	size_t elems[] = {
		24, 56, 94, 13, 25, 26, 27, 8, 37, 29,
		3, 65, 127, 88, 53, 16, 7, 78, 69, 13
	};

	for (size_t elem : elems)
		container.insert(elem, elem);

	Map<size_t, size_t>::InOrderIterator it = container.inbegin();
	Map<size_t, size_t>::InOrderIterator end = container.inend();

	size_t inOrder[] = {
		3, 7, 8, 13, 16, 24, 25, 26, 27, 29,
		37, 53, 56, 65, 69, 78, 88, 94, 127
	};

	size_t i = 0;
	while(it != end) {
		assert(it.value() == inOrder[i++]);
		it.next();
	}
}

void testPostOrderIterator() {
	Map<size_t, size_t> container;
	size_t elems[] = {
		24, 56, 94, 13, 25, 26, 27, 8, 37, 29,
		3, 65, 127, 88, 53, 16, 7, 78, 69, 13
	};

	for (size_t elem : elems)
		container.insert(elem, elem);

	Map<size_t, size_t>::PostOrderIterator it = container.postbegin();
	Map<size_t, size_t>::PostOrderIterator end = container.postend();

	size_t postOrder[] = {
		3, 7, 13, 16, 8, 25, 24, 27, 37, 53,
		29, 26, 65, 69, 88, 78, 127, 94, 56
	};

	size_t i = 0;
	while(it != end) {
		assert(it.value() == postOrder[i++]);
		it.next();
	}
}

int main() {
	TestEngine::execute("1", &testEmpty);
	TestEngine::execute("2", &testFull);
	TestEngine::execute("3", &testInsertDuplicates);
	TestEngine::execute("4", &testInsertAndDelete);
	TestEngine::execute("5", &testInsertAndDeleteArbitrary);
	TestEngine::execute("6", &testClear);
	TestEngine::execute("7", &testSwap);
	TestEngine::execute("8", &testCopyCtor);
	TestEngine::execute("9", &testAsignation);

	TestEngine::execute("10", &testPreOrderIterator);
	TestEngine::execute("11", &testInOrderIterator);
	TestEngine::execute("12", &testPostOrderIterator);
	TestEngine::printResults();
	return 0;
}
