#include "common/common.hpp"
#include "map/hash_map.hpp"


int main() {
	ilc::csv::CsvWriter csv;
	ilc::ads::Map<size_t, size_t> map;

	BENCHMARK_BEGIN("isauro_hash_insert.csv")
	{
		map[i * M + j] = i;
	}
	BENCHMARK_CSV_WRITE() << map.memorySpace() << "\n";
	BENCHMARK_END("insert")

	BENCHMARK_BEGIN("isauro_hash_erase.csv")
	{
		map.erase(i * M + j);
	}
	BENCHMARK_CSV_WRITE() << map.memorySpace() << "\n";
	BENCHMARK_END("erase")
	return 0;
}
