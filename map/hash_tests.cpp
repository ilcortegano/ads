#include "common/testing/engine.hpp"
#include "common/testing/asserts.hpp"
#include "map/hash_map.hpp"
using namespace ilc::test;
using namespace ilc::ads;


template<class K, class V>
class FakeMap {

public:

	struct FakeMapNode {
		K key;
		V value;
		FakeMapNode *next;
	};

	FakeMapNode **array;
	size_t elems;
	size_t capacity;
};


void testEmpty() {
	Map<size_t, size_t> container;

	assert(container.empty());
	assert(!container.full());
	assert(container.size() == 0);
	assert(container.occupancy() == 0);
	assert(container.capacity() == INITIAL_CAPACITY);
	assert_false(container.contains(1));
	assert_exception_thrown(container.at(1));
}

void testFull() {
	Map<size_t, size_t> container;
	FakeMap<size_t, size_t>* manipulator = reinterpret_cast<FakeMap<size_t, size_t>*>(&container);
	manipulator->elems = MAX_CAPACITY;

	assert(!container.empty());
	assert(container.full());
	assert(container.size() == MAX_CAPACITY);
	assert_exception_thrown(container.insert(0, 0));

	manipulator->elems = 0; // Restore the state for the 'free()' function;
}

void testInsertAndErase() {
	const size_t N = 512;
	Map<size_t, size_t> container;

	for(size_t i = 0; i < N; i++) container.insert(i, i);

	assert(!container.empty());
	assert(container.size() == N);
	assert(container.capacity() == N * 2);

	for(size_t i = 0; i < N; i++) {
		assert_true(container.contains(i));
		assert(container.at(i) == i);

		container.erase(i);

		assert_false(container.contains(i));
	}

	assert(container.empty());
	assert(container.size() == 0);
	assert(container.capacity() == N * 2);
}

void testInsertHash() {
	const size_t N = 4;
	Map<size_t, size_t> container;

	for (size_t i = 0; i < N; ++i)
	{
		container.insert(1 + i*INITIAL_CAPACITY, i);
	}

	assert(!container.empty());
	assert(container.size() == N);
	assert(container.at(1) == 0);
	assert(container.at(1+INITIAL_CAPACITY) == 1);
	assert(container.at(1+2*INITIAL_CAPACITY) == 2);
	assert(container.at(1+3*INITIAL_CAPACITY) == 3);
}

void testShrink() {
	size_t N = 256;
	Map<size_t, size_t> containerA;

	for(size_t i = 0; i < N; i++) {
		containerA[i] = i;
	}
	containerA.shrink();

	assert(containerA.size() == N);
	assert(containerA.capacity() == N*2);

	for (size_t i = 0; i < N - 10; i++) containerA.erase(i);
	assert(containerA.size() == 10);
	assert(containerA.capacity() == N*2);

	containerA.shrink();
	assert(containerA.capacity() == N/2);
}

void testClear() {
	const size_t N = INITIAL_CAPACITY + 1;
	Map<size_t, size_t> container;

	for(size_t i = 0; i < N; i++) {
		container.insert(i, i);
	}
	container.clear();

	assert(container.size() == 0);
}

void testSwap() {
	const size_t N = 256;
	Map<size_t, size_t> containerA;
	Map<size_t, size_t> containerB;
	size_t sizeA = rand() % N;
	size_t sizeB = rand() % N;

	for (size_t i = 0; i < sizeA; i++) { containerA.insert(i, i); }
	for (size_t i = 0; i < sizeB; i++) { containerB.insert(sizeB - i, sizeB - i); }
	containerA.swap(containerB);

	assert(containerA.size() == sizeB);
	assert(containerB.size() == sizeA);

	for (size_t i = 0; i < sizeA; i++) { assert_true(containerB.at(i) == i); }
	for (size_t i = 0; i < sizeB; i++) { assert_true(containerA.at(sizeB - i) == sizeB - i); }
}

void testCopyCtor() {
	const size_t N = INITIAL_CAPACITY + 1;
	Map<size_t, size_t> containerA;

	for (size_t i = 0; i < N; i++) containerA.insert(i, i);

	Map<size_t, size_t> containerB(containerA);
	assert(!containerB.full());
	assert(!containerB.empty());
	assert(containerB.size() == N);
	assert(containerB.capacity() == INITIAL_CAPACITY * 2);

	for (size_t i = 0; i < N; i++) {
		assert_true(containerB.contains(i));
		containerB.erase(i);
	}
}

void testAsignation() {
	const size_t N = 256;
	Map<size_t, size_t> containerA;

	for (size_t i = 0; i < N; i++) {
		containerA.insert(i, i);
	}
	containerA = containerA;
	Map<size_t, size_t> containerB = containerA;

	assert(!containerB.full());
	assert(!containerB.empty());
	assert(containerB.size() == N);
	assert(containerB.capacity() == N*2);

	for (size_t i = 0; i < N; i++) {
		assert(containerB.at(i) == containerA.at(i));
		containerB.erase(i);
		containerA.erase(i);
	}
	assert(containerA.empty());
	assert(containerB.empty());
}

int main() {
	TestEngine::execute("1", &testEmpty);
	TestEngine::execute("2", &testFull);
	TestEngine::execute("3", &testInsertAndErase);
	TestEngine::execute("4", &testInsertHash);
	TestEngine::execute("5", &testShrink);
	TestEngine::execute("6", &testClear);
	TestEngine::execute("7", &testSwap);
	TestEngine::execute("8", &testCopyCtor);
	TestEngine::execute("9", &testAsignation);
	TestEngine::printResults();
	return 0;
}
