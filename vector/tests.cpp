#include "common/testing/engine.hpp"
#include "common/testing/asserts.hpp"
#include "vector/vector.hpp"
using namespace ilc::test;
using namespace ilc::ads;

template<typename T>
struct FakeVector {
	T* array;
	size_t elems;
	size_t capacity;
};


void testEmpty() {
	Vector<size_t> container;

	assert(container.empty());
	assert(!container.full());
	assert(container.size() == 0);
	assert(container.capacity() == INITIAL_CAPACITY);

	container.pop();
	assert(container.size() == 0);
}

void testFull() {
	Vector<size_t> container;
	FakeVector<size_t>* manipulator = reinterpret_cast<FakeVector<size_t>*>(&container);
	manipulator->elems = MAX_CAPACITY;

	assert(!container.empty());
	assert(container.full());
	assert(container.size() == MAX_CAPACITY);
	assert_exception_thrown(container.push(0));
}

void testPushAndPop() {
	const size_t N = INITIAL_CAPACITY + 1;
	Vector<size_t> container;

	for(size_t i = 0; i < N; i++) {
		container.push(i);
	}
	assert(!container.empty());
	assert(container.size() == N);
	assert(container.capacity() > INITIAL_CAPACITY);

	for(size_t i = 1; i <= N; i++) {
		container.pop();
	}
	assert(container.empty());
	assert(container.size() == 0);
}

void testFit() {
	const size_t N = INITIAL_CAPACITY + 1;
	Vector<size_t> container;

	for(size_t i = 0; i < N; i++) {
		container.push(i);
	}
	container.fit();

	assert(container.size() == N);
	assert(container.capacity() == N);
}

void testClear() {
	const size_t N = INITIAL_CAPACITY + 1;
	Vector<size_t> container;

	for(size_t i = 0; i < N; i++) {
		container.push(i);
	}
	container.clear();

	assert(container.size() == 0);
}

void testSwap() {
	const size_t N = 256;
	Vector<size_t> containerA;
	Vector<size_t> containerB;
	size_t sizeA = rand() % N;
	size_t sizeB = rand() % N;

	for (size_t i = 0; i < sizeA; i++) { containerA.push(i); }
	for (size_t i = 0; i < sizeB; i++) { containerB.push(sizeB - i); }
	containerA.swap(containerB);

	assert(containerA.size() == sizeB);
	assert(containerB.size() == sizeA);
}

void testCopyCtor() {
	const size_t N = INITIAL_CAPACITY + 1;
	Vector<size_t> containerA;

	for (size_t i = 0; i < N; i++) {
		containerA.push(i);
		assert(containerA[i] == i);
	}

	Vector<size_t> containerB(containerA);
	assert(!containerB.full());
	assert(!containerB.empty());
	assert(containerB.size() == N);

	for (size_t i = 0; i < N; i++) {
		containerB.pop();
	}
}

void testAsignation() {
	const size_t N = INITIAL_CAPACITY + 1;
	Vector<size_t> containerA;

	for (size_t i = 0; i < N; i++) {
		containerA.push(i);
	}
	containerA = containerA;
	Vector<size_t> containerB = containerA;

	assert(!containerB.full());
	assert(!containerB.empty());
	assert(containerB.size() == N);

	for (size_t i = 0; i < N; i++) {
		containerB.pop();
		containerA.pop();
	}
}

void testEquals() {
	Vector<size_t> containerA, containerB;

	assert(containerA == containerB);
	assert(!(containerA != containerB));

	containerA.push(0);
	assert(containerA != containerB);

	containerB.push(0);
	assert(containerA == containerB);

	containerA.push(1);
	assert(containerA != containerB);

	containerB.pop();
	containerB.push(0);
	containerB.push(1);
	assert(containerA == containerB);
}

void testAlphanumericOrder() {
	Vector<size_t> containerA, containerB;

	assert(containerA >= containerB);
	assert(containerA <= containerB);
	assert(!(containerA < containerB));
	assert(!(containerA > containerB));

	containerA.push(0);
	assert(containerA > containerB);

	containerB.push(1);
	assert(containerB > containerA);

	containerA.pop();
	containerA.push(0);
	containerA.push(1);
	assert(containerA > containerB);

	containerB.clear();
	containerB.push(0);
	containerB.push(1);
	assert(!(containerA > containerB));
	assert(!(containerA < containerB));
	assert(containerA == containerB);
}

int main() {
	TestEngine::execute("1", &testEmpty);
	TestEngine::execute("2", &testFull);
	TestEngine::execute("3", &testPushAndPop);
	TestEngine::execute("4", &testFit);
	TestEngine::execute("5", &testClear);
	TestEngine::execute("6", &testSwap);
	TestEngine::execute("7", &testCopyCtor);
	TestEngine::execute("8", &testAsignation);
	TestEngine::execute("9", &testEquals);
	TestEngine::execute("10", &testAlphanumericOrder);
	TestEngine::printResults();
	return 0;
}
