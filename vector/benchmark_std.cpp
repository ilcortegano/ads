#include "common/common.hpp"
#include <vector>


int main() {
	ilc::csv::CsvWriter csv;
	std::vector<size_t> vector;

	BENCHMARK_BEGIN("std_push.csv")
	{
		vector.push_back(i);
	}
	BENCHMARK_STD_CSV_WRITE()
	BENCHMARK_END("push")

	BENCHMARK_BEGIN("std_pop.csv")
	{
		vector.pop_back();
	}
	BENCHMARK_STD_CSV_WRITE()
	BENCHMARK_END("pop")
	return 0;
}
