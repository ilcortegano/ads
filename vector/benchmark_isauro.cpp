#include "common/common.hpp"
#include "vector/vector.hpp"


int main() {
	ilc::csv::CsvWriter csv;
	ilc::ads::Vector<size_t> vector;

	BENCHMARK_BEGIN("isauro_push.csv")
	{
		vector.push(i);
	}
	BENCHMARK_CSV_WRITE() << vector.memorySpace() << "\n";
	BENCHMARK_END("push")

	BENCHMARK_BEGIN("isauro_pop.csv")
	{
		vector.pop();
	}
	BENCHMARK_CSV_WRITE() << vector.memorySpace() << "\n";
	BENCHMARK_END("pop")
	return 0;
}
