#include "common/common.hpp"
#include <queue>


int main() {
	const size_t ADS_SIZE = sizeof(std::priority_queue<int>);
	ilc::csv::CsvWriter csv;
	std::priority_queue<int> heap;

	BENCHMARK_BEGIN("std_push.csv")
	{
		heap.push(0);
	}
	BENCHMARK_STD_CSV_WRITE()
	BENCHMARK_END("push")

	BENCHMARK_BEGIN("std_pop.csv")
	{
		heap.pop();
	}
	BENCHMARK_STD_CSV_WRITE()
	BENCHMARK_END("pop")
	return 0;
}
