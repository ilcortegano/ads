#pragma once
#include <stdexcept>

namespace ilc::ads {

	constexpr size_t INITIAL_CAPACITY = 8;
	constexpr size_t MAX_CAPACITY = 9223372036854775808ULL;


	template <class T>
	class Heap {

	public:
		//------------------------------------//
		//     CONSTRUCTORS & DESTRUCTOR      //
		//------------------------------------//
		Heap(): _array(new T[INITIAL_CAPACITY]), _capacity(INITIAL_CAPACITY), _elems(0) {}

		Heap(const Heap &other) {
			copy(other);
		}

		~Heap() {
			delete[] _array;
		}

		//------------------------------------//
		//             MODIFIERS              //
		//------------------------------------//
		void push(const T &elem) {
			if (_elems == MAX_CAPACITY) throw std::overflow_error("");
			if (_elems == _capacity)
				resize(_capacity << 1);

			size_t pos {_elems};
			size_t middle {(_elems-1) / 2};

			while (pos > 0 && elem < _array[middle]) {

				_array[pos] = _array[middle];
				pos = middle;
				middle = (middle-1) / 2;
			}

			_array[pos] = elem;
			_elems++;
		}

		void pop() {
			if (_elems == 0) throw std::out_of_range("");

			size_t hole {0}, son {2};
			bool heap {false};

			_array[hole] = _array[--_elems];
			while (son <= _elems && !heap) {

				son += son != _elems && _array[son] < _array[son - 1];

				if (_array[hole] > _array[son - 1]) {
					T aux {_array[hole]};

					_array[hole] = _array[son - 1];
					hole = son - 1;
					_array[hole] = aux;
					son *= 2;
				}
				else {
					heap = true;
				}
			}

			if (_elems < (_capacity >> 2))
				resize(_capacity >> 1);
		}

		void fit() {
			if (_elems == 0)
				return;

			resize(_elems);
		}

		void clear() {
			_elems = 0;
		}

		void swap(Heap& other) {
			T* array {_array};
			size_t capacity {_capacity};
			size_t elems {_elems};

			_array = other._array;
			_capacity = other._capacity;
			_elems = other._elems;

			other._array = array;
			other._capacity = capacity;
			other._elems = elems;
		}

		//------------------------------------//
		//             OBSERVERS              //
		//------------------------------------//
		const T &next() const {
			if (_elems == 0) throw std::out_of_range("");
			return _array[0];
		}

		size_t size() const {
			return _elems;
		}

		size_t capacity() const {
			return _capacity;
		}

		size_t memorySpace() const {
			return sizeof(Heap) + sizeof(T) * _capacity;
		}

		bool empty() const {
			return _elems == 0;
		}

		bool full() const {
			return _elems == MAX_CAPACITY;
		}

		//------------------------------------//
		//             OPERATORS              //
		//------------------------------------//
		Heap &operator=(const Heap &other) {
			if (this != &other) {
				Heap tmp{other};
				swap(tmp);
			}
			return *this;
		}

	private:
		T *_array;
		size_t _elems;
		size_t _capacity;

		void resize(size_t size) {
			T *old = _array;
			_capacity = size;
			_array = new T[size];

			memcpy(_array, old, _elems * sizeof(T));
			delete[] old;
		}

		void copy(const Heap &other) {
			_elems = other._elems;
			_capacity = other._capacity;

			_array = new T[other._capacity];
			memcpy(_array, other._array, other._elems * sizeof(T));
		}
	};
};