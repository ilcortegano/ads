#include "common/testing/engine.hpp"
#include "common/testing/asserts.hpp"
#include "heap/heap.hpp"
using namespace ilc::test;
using namespace ilc::ads;

template<typename T>
struct FakeHeap {
	T* array;
	size_t elems;
	size_t capacity;
};


void testEmpty() {
	Heap<size_t> container;

	assert(container.empty());
	assert(!container.full());
	assert(container.size() == 0);
	assert(container.capacity() == INITIAL_CAPACITY);
	assert_exception_thrown(container.next());
	assert_exception_thrown(container.pop());
}

void testFull() {
	Heap<size_t> container;
	FakeHeap<size_t>* manipulator = reinterpret_cast<FakeHeap<size_t>*>(&container);
	manipulator->elems = MAX_CAPACITY;

	assert(!container.empty());
	assert(container.full());
	assert(container.size() == MAX_CAPACITY);
	assert_exception_thrown(container.push(0));
}

void testPushAndPop() {
	const size_t N = 256;
	Heap<size_t> container;

	for(size_t i = 0; i < N; i++) {
		container.push(i);
		assert(container.next() == 0);
	}
	assert(!container.empty());
	assert(container.size() == N);
	assert(container.capacity() == N);

	for(size_t i = 0; i < N; i++) {
		assert(container.next() == i);
		container.pop();
	}
	assert(container.empty());
	assert(container.size() == 0);
	assert(container.capacity() == 2);
}

void testFit() {
	const size_t N = INITIAL_CAPACITY + 1;
	Heap<size_t> container;

	for(size_t i = 0; i < N; i++) {
		container.push(i);
	}
	container.fit();

	assert(container.size() == N);
	assert(container.capacity() == N);
}

void testClear() {
	const size_t N = INITIAL_CAPACITY + 1;
	Heap<size_t> container;

	for(size_t i = 0; i < N; i++) {
		container.push(i);
	}
	container.clear();

	assert(container.size() == 0);
	assert(container.capacity() == (INITIAL_CAPACITY * 2));
}

void testSwap() {
	const size_t N = 256;
	Heap<size_t> containerA;
	Heap<size_t> containerB;
	size_t sizeA = rand() % N;
	size_t sizeB = rand() % N;

	for (size_t i = 0; i < sizeA; i++) { containerA.push(i); }
	for (size_t i = 0; i < sizeB; i++) { containerB.push(sizeB - i); }
	containerA.swap(containerB);

	assert(containerA.size() == sizeB);
	assert(containerA.next() ==  1);
	assert(containerB.size() == sizeA);
	assert(containerB.next() == 0);
}

void testCopyCtor() {
	const size_t N = INITIAL_CAPACITY + 1;
	Heap<size_t> containerA;

	for (size_t i = 0; i < N; i++) {
		containerA.push(i);
	}

	Heap<size_t> containerB(containerA);
	assert(!containerB.full());
	assert(!containerB.empty());
	assert(containerB.size() == N);
	assert(containerB.capacity() == INITIAL_CAPACITY * 2);

	for (size_t i = 0; i < N; i++) {
		assert(containerB.next() == i);
		containerB.pop();
	}
}

void testAsignation() {
	const size_t N = INITIAL_CAPACITY + 1;
	Heap<size_t> containerA;

	for (size_t i = 0; i < N; i++) {
		containerA.push(i);
	}
	containerA = containerA;
	Heap<size_t> containerB = containerA;

	assert(!containerB.full());
	assert(!containerB.empty());
	assert(containerB.size() == N);
	assert(containerB.capacity() == INITIAL_CAPACITY * 2);

	for (size_t i = 0; i < N; i++) {
		assert(containerB.next() == containerA.next());
		containerB.pop();
		containerA.pop();
	}
}

int main() {
	TestEngine::execute("1", &testEmpty);
	TestEngine::execute("2", &testFull);
	TestEngine::execute("3", &testPushAndPop);
	TestEngine::execute("4", &testFit);
	TestEngine::execute("5", &testClear);
	TestEngine::execute("6", &testSwap);
	TestEngine::execute("7", &testCopyCtor);
	TestEngine::execute("8", &testAsignation);
	TestEngine::printResults();
	return 0;
}
