#include "common/common.hpp"
#include "heap/heap.hpp"


int main() {
	ilc::csv::CsvWriter csv;
	ilc::ads::Heap<int> heap;

	BENCHMARK_BEGIN("isauro_push.csv")
	{
		heap.push(0);
	}
	BENCHMARK_CSV_WRITE() << heap.memorySpace() << "\n";
	BENCHMARK_END("push")

	BENCHMARK_BEGIN("isauro_pop.csv")
	{
		heap.pop();
	}
	BENCHMARK_CSV_WRITE() << heap.memorySpace() << "\n";
	BENCHMARK_END("pop")
	return 0;
}
