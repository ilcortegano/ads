#include "common/testing/engine.hpp"
#include "common/testing/asserts.hpp"
#include "list/list.hpp"
using namespace ilc::test;
using namespace ilc::ads;


template<class T>
class FakeList {

public:

	struct FakeListNode {
		T elem;
		FakeListNode *next;
		FakeListNode *prev;
	};

	FakeListNode *first;
	FakeListNode *last;
	size_t elems;
};


void testEmpty() {
	List<size_t> container;

	assert(container.empty());
	assert(!container.full());
	assert(container.size() == 0);

	assert_exception_thrown(container.front());
	assert_exception_thrown(container.pop_front());

	assert_exception_thrown(container.back());
	assert_exception_thrown(container.pop_back());
}

void testFull() {
	List<size_t> container;
	FakeList<size_t>* manipulator = reinterpret_cast<FakeList<size_t>*>(&container);
	manipulator->elems = SIZE_MAX;

	assert(!container.empty());
	assert(container.full());
	assert(container.size() == SIZE_MAX);

	assert_exception_thrown(container.push_front(0));
	assert_exception_thrown(container.push_back(0));
}

void testPushBackAndPopFront() {
	const size_t N = 512;
	List<size_t> container;

	for(size_t i = 0; i < N; i++) container.push_back(i);

	assert(!container.empty());
	assert(container.size() == N);
	assert(container.back() == N - 1);

	for(size_t i = 0; i < N; i++) {
		assert(container.front() == i);
		container.pop_front();
	}

	assert(container.empty());
	assert(container.size() == 0);
}

void testPushFrontAndPopBack() {
	const size_t N = 512;
	List<size_t> container;

	for(size_t i = 0; i < N; i++) container.push_front(i);

	assert(!container.empty());
	assert(container.size() == N);
	assert(container.front() == N - 1);

	for(size_t i = 0; i < N; i++) {
		assert(container.back() == i);
		container.pop_back();
	}

	assert(container.empty());
	assert(container.size() == 0);
}

void testClear() {
	const size_t N = 36;
	List<size_t> container;

	for(size_t i = 0; i < N; i++)
		container.push_back(i);

	assert(!container.empty());
	assert(container.size() == N);
	assert(container.front() == 0);
	assert(container.back() == N - 1);

	container.clear();

	assert(container.empty());
	assert(container.size() == 0);
	assert_exception_thrown(container.front());
	assert_exception_thrown(container.back());
}

void testSwap() {
	const size_t N = 256;
	List<size_t> containerA;
	List<size_t> containerB;
	size_t sizeA = rand() % N;
	size_t sizeB = rand() % N;

	for (size_t i = 0; i < sizeA; i++) { containerA.push_back(i); }
	for (size_t i = 0; i < sizeB; i++) { containerB.push_front(sizeB - i); }
	containerA.swap(containerB);

	assert(containerA.size() == sizeB);
	assert(containerB.size() == sizeA);

	List<size_t>::ConstIterator cbegin = containerB.cbegin();
	List<size_t>::ConstIterator cend = containerB.cend();
	size_t i = 0;
	while(cbegin != cend) {
		assert(cbegin.elem() == i);
		cbegin++;
		i++;
	}

	cbegin = containerA.rcbegin();
	cend = containerA.rcend();
	i = 0;
	while(cbegin != cend) {
		assert(cbegin.elem() == sizeB - i);
		cbegin--;
		i++;
	}
}

void testCopyCtor() {
	const size_t N = 36;
	List<size_t> containerA;

	for (size_t i = 0; i < N; i++) containerA.push_back(i);

	List<size_t> containerB(containerA);

	assert(!containerB.full());
	assert(!containerB.empty());
	assert(containerB.size() == N);

	List<size_t>::ConstIterator cbegin = containerB.cbegin();
	List<size_t>::ConstIterator cend = containerB.cend();
	size_t i = 0;
	while(cbegin != cend) {
		assert(cbegin.elem() == i);
		cbegin++;
		i++;
	}
}

void testAsignation() {
	const size_t N = 36;
	List<size_t> containerA;

	for (size_t i = 0; i < N; i++) containerA.push_back(i);
	containerA = containerA;
	List<size_t> containerB = containerA;

	assert(!containerB.full());
	assert(!containerB.empty());
	assert(containerB.size() == containerA.size());

	List<size_t>::ConstIterator cbeginA = containerA.cbegin();
	List<size_t>::ConstIterator cbeginB = containerB.cbegin();
	List<size_t>::ConstIterator cendB = containerB.cend();
	while(cbeginB != cendB) {
		assert(cbeginB.elem() == cbeginA.elem());
		cbeginB++;
		cbeginA++;
	}
}

void testIteratorOps() {
	List<size_t> container;

	List<size_t>::Iterator begin = container.begin();
	container.insert(1, begin);

	assert(container.find(0) == container.end());
	assert(container.find(1) == container.begin());

	List<size_t>::Iterator it = container.find(1);
	it.set(2);
	assert(it.elem() == 2);
	assert(container.find(2) != container.end());

	assert(it == container.begin());
	it = container.erase(it);
	assert(it == container.end());
	assert_exception_thrown(it.elem());
}

int main() {
	TestEngine::execute("1", &testEmpty);
	TestEngine::execute("2", &testFull);
	TestEngine::execute("3", &testPushBackAndPopFront);
	TestEngine::execute("4", &testPushFrontAndPopBack);
	TestEngine::execute("5", &testClear);
	TestEngine::execute("6", &testSwap);
	TestEngine::execute("7", &testCopyCtor);
	TestEngine::execute("8", &testAsignation);
	TestEngine::execute("9", &testIteratorOps);
	TestEngine::printResults();
	return 0;
}
