#include "common/common.hpp"
#include "list/list.hpp"


int main() {
	ilc::csv::CsvWriter csv;
	ilc::ads::List<size_t> list;

	BENCHMARK_BEGIN("isauro_push.csv")
	{
		list.push_back(i);
	}
	BENCHMARK_CSV_WRITE() << list.memorySpace() << "\n";
	BENCHMARK_END("push")

	BENCHMARK_BEGIN("isauro_pop.csv")
	{
		list.pop_back();
	}
	BENCHMARK_CSV_WRITE() << list.memorySpace() << "\n";
	BENCHMARK_END("pop")
	return 0;
}
