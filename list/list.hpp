#pragma once
#include <stdexcept>

namespace ilc::ads {

	template <class T>
	class List {

	private:
		class Node {

		public:
			Node(Node *prev, const T &elem, Node *next) : _elem(elem), _next(next), _prev(prev) {};

			T _elem;
			Node *_next;
			Node *_prev;
		}; 

	public:
		//------------------------------------//
		//     CONSTRUCTORS & DESTRUCTOR      //
		//------------------------------------//
		List(): _first(nullptr), _last(nullptr), _elems(0) {}

		List(const List &other): List() {
			copy(other);
		}

		~List() {
			free();
		}

		//------------------------------------//
		//             MODIFIERS              //
		//------------------------------------//
		void push_front(const T &elem) {
			if (_elems == SIZE_MAX) throw std::overflow_error("");

			_first = insertNode(elem, nullptr, _first);
			if (!_last)
				_last = _first;
		}

		void pop_front() {
			if (!_first) throw std::out_of_range("");
			Node *newFirst =  _first->_next;

			deleteNode(_first);
			_first = newFirst;
			if (!newFirst)
				_last = nullptr;
		}

		void push_back(const T &elem) {
			if (_elems == SIZE_MAX) throw std::overflow_error("");

			_last = insertNode(elem, _last, nullptr);
			if (!_first)
				_first = _last;
		}

		void pop_back() {
			if (!_last) throw std::out_of_range("");
			Node *newLast = _last->_prev;

			deleteNode(_last);
			_last = newLast;
			if (!newLast)
				_first = nullptr;
		}

		void clear() {
			free();
			_elems = 0;
		}

		void swap(List &other) {
			Node *first {_first};
			Node *last {_last};
			size_t elems {_elems};

			_first = other._first;
			_last = other._last;
			_elems = other._elems;

			other._first = first;
			other._last = last;
			other._elems = elems;
		}

		//------------------------------------//
		//             OBSERVERS              //
		//------------------------------------//
		const T &front() const {
			if (!_first) throw std::runtime_error("");
			return _first->_elem;
		}

		const T &back() const {
			if (!_last) throw std::runtime_error("");
			return _last->_elem;
		}

		size_t size() const {
			return _elems;
		}

		size_t memorySpace() const {
			return sizeof(List) + sizeof(Node) * _elems;
		}

		bool empty() const {
			return _elems == 0;
		}

		bool full() const {
			return _elems == SIZE_MAX;
		}

		//------------------------------------//
		//             OPERATORS              //
		//------------------------------------//
		List &operator=(const List &other) {
			if (this != &other) {
				List tmp{other};
				swap(tmp);
			}
			return *this;
		}

		//------------------------------------//
		//          CONST-ITERATOR            //
		//------------------------------------//
		class ConstIterator {
		public:
			void next() {
				if (!_curr) throw std::out_of_range("");
				_curr = _curr->_next;
			}

			void prev() {
				if (!_curr) throw std::out_of_range("");
				_curr = _curr->_prev;
			}
			
			const T &elem() const {
				if (!_curr) throw std::out_of_range("");
				return _curr->_elem;
			}
			
			bool operator==(const ConstIterator &other) const {
				return _curr == other._curr;
			}
			
			bool operator!=(const ConstIterator &other) const {
				return !(this->operator==(other));
			}

			const T &operator*() const {
				return elem();
			}

			ConstIterator &operator++() {
				next();
				return *this;
			}

			ConstIterator operator++(int) {
				ConstIterator ret(*this);
				operator++();
				return ret;
			}

			ConstIterator &operator--() {
				prev();
				return *this;
			}

			ConstIterator operator--(int) {
				ConstIterator ret(*this);
				operator--();
				return ret;
			}

		protected:
			friend class List;

			ConstIterator(Node* curr): _curr(curr) {}

			Node *_curr;
		};

		ConstIterator cbegin() const {
			return ConstIterator(_first);
		}

		ConstIterator rcbegin() const {
			return ConstIterator(_last);
		}

		ConstIterator cend() const {
			return ConstIterator(nullptr);
		}

		ConstIterator rcend() const {
			return ConstIterator(nullptr);
		}

		//------------------------------------//
		//             ITERATOR               //
		//------------------------------------//
		class Iterator: public ConstIterator {
		public:

			void set(const T &elem) const {
				if (!this->_curr) throw std::out_of_range("");
				this->_curr->_elem = elem;
			}

		private:
			friend class List;

			Iterator(Node *curr): ConstIterator(curr) {}
		};

		Iterator begin() const {
			return Iterator(_first);
		}

		Iterator rbegin() const {
			return Iterator(_last);
		}

		Iterator end() const {
			return Iterator(nullptr);
		}

		Iterator rend() const {
			return Iterator(nullptr);
		}

		Iterator at(size_t pos) const {
			if (pos >= _elems) throw std::out_of_range("");

			Node *curr = _first;
			for (size_t i = 0; i < pos; ++i)
				curr = curr->_next;

			return Iterator(curr);
		}

		Iterator find(const T &elem) const {
			if (!_first) throw std::runtime_error("");

			Node *curr = _first;
			while (curr && curr->_elem != elem)
				curr = curr->_next;

			return Iterator(curr);
		}

		Iterator erase(const Iterator &it) {
			if (!it._curr) throw std::out_of_range("");

			if (it._curr == _first) {
				pop_front();
				return Iterator(_first);
			}

			if (it._curr == _last) {
				pop_back();
				return Iterator(nullptr);
			}

			Node *curr = it._curr->_next;
			deleteNode(it._curr);
			return Iterator(curr);
		}

		void insert(const T &elem, const Iterator &it) {

			if (it._curr == _first) {
				push_front(elem);
			} else
			if (!it._curr) {
				push_back(elem);
			}
			else {
				insertNode(elem, it._curr->_prev, it._curr);
			}
		}

	private:
		Node *_first;
		Node *_last;
		size_t _elems;

		Node *insertNode(const T &elem, Node *nodeA, Node *nodeB) {
			Node *newNode = new Node(nodeA, elem, nodeB);

			if (nodeA)
				nodeA->_next = newNode;
			if (nodeB)
				nodeB->_prev = newNode;
			++_elems;
			return newNode;
		}

		void deleteNode(Node *node) {
			Node *prev = node->_prev;
			Node *next = node->_next;
			delete node;

			if (prev)
				prev->_next = next;
			if (next)
				next->_prev = prev;
			--_elems;
		}

		void free() {
			while (_first) {
				Node *toDelete = _first;
				_first = _first->_next;
				delete toDelete;
			}
			_last = nullptr;
		}

		void copy(const List &other) {
			Node *curr = other._first;

			while (curr) {
				push_back(curr->_elem);
				curr = curr->_next;
			}
		}
	};
};