#include "common/common.hpp"
#include <list>


int main() {
	ilc::csv::CsvWriter csv;
	std::list<size_t> list;

	BENCHMARK_BEGIN("std_push.csv")
	{
		list.push_back(i);
	}
	BENCHMARK_STD_CSV_WRITE()
	BENCHMARK_END("push")

	BENCHMARK_BEGIN("std_pop.csv")
	{
		list.pop_back();
	}
	BENCHMARK_STD_CSV_WRITE()
	BENCHMARK_END("pop")
	return 0;
}
