import os
import csv
import matplotlib.pyplot as plt
from dataclasses import dataclass, field


class Config:
	N = 300
	M = 1000
	TIME_FILTER = None
	STD_TIME_FILTER = None
	ADS = ''
	STD_ADS = ''
	MATH_FN = ''
	MATH_FN2 = ''
	OPERATION = ''
	STD_OPERATION = ''
	BASE_PATH = ''

@dataclass
class AdsBenchmarkData:
	index: list[int] = field(default_factory=list)
	time: list[int]  = field(default_factory=list)
	space: list[int] = field(default_factory=list)

@dataclass
class Benchmark:
	data: AdsBenchmarkData
	color: str
	label: str


def extract_csv_data(file: str) -> AdsBenchmarkData | Exception:
	data = AdsBenchmarkData()

	with open(file) as file:
		reader = csv.reader(file, delimiter=',')
	
		for i, row in enumerate(reader):
			data.index.append(i)
			data.time.append(float(row[0]))
			data.space.append(float(row[1]))

	return data

def filter_csv_time_data(data: AdsBenchmarkData, threshold: int) -> None:
	data.time = list(map(lambda time: threshold if time >= threshold else time, data.time))

def build_std_benchmark(config: Config) -> Benchmark:
	file = os.path.join(config.BASE_PATH, f'std_{config.STD_OPERATION}.csv')
	DATA = extract_csv_data(file)
	if config.STD_TIME_FILTER is not None:
		filter_csv_time_data(DATA, config.STD_TIME_FILTER)
	return Benchmark(DATA, 'g', f"std::{config.STD_ADS}")

def build_math_benchmark(config: Config) -> Benchmark:
	file = os.path.join(config.BASE_PATH, f'{config.MATH_FN}.csv')
	DATA = extract_csv_data(file)
	return Benchmark(DATA, 'r', f"{config.MATH_FN}")

def build_math2_benchmark(config: Config) -> Benchmark:
	file = os.path.join(config.BASE_PATH, f'{config.MATH_FN2}.csv')
	DATA = extract_csv_data(file)
	return Benchmark(DATA, 'y', f"{config.MATH_FN2}")

def build_ilc_benchmark(config: Config) -> Benchmark:
	file = os.path.join(config.BASE_PATH, f'isauro_{config.OPERATION}.csv')
	DATA = extract_csv_data(file)
	if config.TIME_FILTER is not None:
		filter_csv_time_data(DATA, config.TIME_FILTER)
	return Benchmark(DATA, 'b', f"ilc::ads::{config.ADS}")

def decorate_chart(config: Config, label: str) -> None:
	plt.xlabel(f'elements (x{config.M})')
	plt.ylabel(label)
	plt.title(f'"{config.OPERATION}" benchmark; N = {(config.N * config.M):,}')
	plt.grid()
	plt.legend()

def add_time_data_to_chart(b: Benchmark) -> None:
	plt.plot(b.data.index, b.data.time, color = b.color, linestyle = 'dashed', label = b.label)

def add_space_data_to_chart(b: Benchmark) -> None:
	plt.plot(b.data.index, b.data.space, color = b.color, linestyle = 'dashed', label = b.label)

def plot(config: Config) -> None:
	ILC = build_ilc_benchmark(config)

	add_time_data_to_chart(ILC)
	if config.STD_ADS:
		STD = build_std_benchmark(config)
		add_time_data_to_chart(STD)
	if config.MATH_FN:
		MATH = build_math_benchmark(config)
		add_time_data_to_chart(MATH)
	if config.MATH_FN2:
		MATH = build_math2_benchmark(config)
		add_time_data_to_chart(MATH)
	decorate_chart(config, 'time (ns)')
	plt.show()

	add_space_data_to_chart(ILC)
	decorate_chart(config, 'bytes')
	plt.show()