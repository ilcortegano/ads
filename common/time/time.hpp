#pragma once
#include <string>
#include <chrono>
#include <iostream>

typedef std::chrono::high_resolution_clock Time;
typedef std::chrono::duration<double> sec;
typedef std::chrono::milliseconds ms;
typedef std::chrono::microseconds us;
typedef std::chrono::nanoseconds ns;


void showTimeDetails(const std::string& op, size_t n, const sec& sec) {
	ms msec = std::chrono::duration_cast<ms>(sec);
	us usec = std::chrono::duration_cast<us>(sec);

	std::cout << "'" << op << "' operation; executing " << n << " times." << std::endl;
	std::cout << "\tT(s): " << sec.count() << std::endl;
	std::cout << "\tT(ms): " << msec.count() << std::endl;
	std::cout << "\tT(us): " << usec.count() << std::endl;
}