#include "common/testing/engine.hpp"

namespace ilc::test {

	size_t TestEngine::failCount = 0;

	void TestEngine::execute(std::string name, void (*function)(void)) {
		try {
			function();
		} catch (std::exception& e) {
			failCount++;
			std::cout << "-> " << name << ": KO" << std::endl;
		}
	}

	void TestEngine::printResults() {
		if (failCount == 0)
			std::cout << "-> All tests passed." << std::endl;
		else
			std::cout << "-> " << failCount << " tests have failed." << std::endl;
	}
};
