#include <cassert>

bool throws = false;

#define assert_exception_thrown(myCode)   \
throws = false;                           \
try { myCode; }                           \
catch(const std::exception&) { throws = true; } \
assert(throws);

#define assert_exception_not_thrown(myCode) \
throws = false;                             \
try { myCode; }                             \
catch(const std::exception&) { throws = true; } \
assert(!throws);

#define assert_true(myCode) assert(myCode);

#define assert_false(myCode) assert(!myCode);

#define log_debug(myCode) std::cout << myCode << std::endl;
