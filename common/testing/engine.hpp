#pragma once
#include <iostream>
#include <cassert>


namespace ilc::test {

	class TestEngine {

	public:
		static void execute(std::string testName, void (*function)(void));
		static void printResults();

	private:
		static size_t failCount;
	};
};
