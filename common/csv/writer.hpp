#pragma once
#include <fstream>
#include <chrono>


namespace ilc::csv {

	class CsvWriter {

	public:

		//------------------------------------//
		//     CONSTRUCTORS & DESTRUCTOR      //
		//------------------------------------//
		CsvWriter();

		CsvWriter(CsvWriter const&)      = delete;
		void operator=(CsvWriter const&) = delete;

		//------------------------------------//
		//             MODIFIERS              //
		//------------------------------------//
		bool open(const std::string& name);
		void close();

		CsvWriter& operator<<(bool val);
		CsvWriter& operator<<(int val);
		CsvWriter& operator<<(size_t val);
		CsvWriter& operator<<(double val);
		CsvWriter& operator<<(const char* val);
		CsvWriter& operator<<(const std::string& val);
		CsvWriter& operator<<(const std::chrono::_V2::system_clock::rep& val);

	private:
		std::ofstream _file{};
	};
};
