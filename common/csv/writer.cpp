#include "common/csv/writer.hpp"

namespace ilc::csv {

	CsvWriter::CsvWriter() {}

	bool CsvWriter::open(const std::string& name) {
		std::ios_base::openmode mode =  std::ios_base::out | std::ios_base::trunc;

		_file.open(name, mode);
		return _file.good();
	}

	void CsvWriter::close() {
		_file.close();
	}

	CsvWriter& CsvWriter::operator<<(bool val) {
		_file << val;
		return *this;
	}

	CsvWriter& CsvWriter::operator<<(int val) {
		_file << val;
		return *this;
	}

	CsvWriter& CsvWriter::operator<<(size_t val) {
		_file << val;
		return *this;
	}

	CsvWriter& CsvWriter::operator<<(double val) {
		_file << val;
		return *this;
	}

	CsvWriter& CsvWriter::operator<<(const char* val) {
		_file << val;
		return *this;
	}

	CsvWriter& CsvWriter::operator<<(const std::string& val) {
		_file << val;
		return *this;
	}

	CsvWriter& CsvWriter::operator<<(const std::chrono::_V2::system_clock::rep& val) {
		_file << val;
		return *this;
	}
};
