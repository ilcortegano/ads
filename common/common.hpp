#include <iostream>
#include "common/time/time.hpp"
#include "common/csv/writer.hpp"
using namespace std;


constexpr size_t N = 300;
constexpr size_t M = 1000;

auto g0 = Time::now();
auto g1 = Time::now();

#define BENCHMARK_MATH_FOO_BEGIN(csvName)      \
csv.open(csvName);                             \
for (size_t i = 0; i < N; i++) {               \

#define BENCHMARK_MATH_FOO_WRITE(insertValue, eraseValue) \
	csv << insertValue << "," << eraseValue << "\n";

#define BENCHMARK_MATH_FOO_END() \
}                                \
csv.close();


#define BENCHMARK_BEGIN(csvName)               \
csv.open(csvName);                             \
g0 = Time::now();                              \
for (size_t i = 0; i < N; i++) {               \
	auto t0 = Time::now();                     \
	for (size_t j = 0; j < M; j++)

#define BENCHMARK_CSV_WRITE()                  \
	auto t1 = Time::now();                     \
	sec elapsed = t1 - t0;                     \
	ns d = chrono::duration_cast<ns>(elapsed); \
	csv << d.count() << ","

#define BENCHMARK_STD_CSV_WRITE()              \
	BENCHMARK_CSV_WRITE() << 0 << "\n";

#define BENCHMARK_END(name)                    \
}                                              \
g1 = Time::now();                              \
csv.close();                                   \
showTimeDetails(name, N * M, g1 - g0);
